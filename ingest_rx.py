import sys
import re
import pandas as pd

if __name__ == "__main__":
    fn = sys.argv[1]
    with open(fn, 'r') as f:
        lines = f.readlines()

    line_count = 0
    data = []
    while line_count < len(lines):
        line = lines[line_count].strip()
        line_count += 1
        print (line)
        if len(line) == 0: continue
        if line.startswith("Do") or line.startswith('\x00'):
            pass
        else:
            tuples = re.split('\s+',line)
            print (tuples)
            tuples = [int(t) for t in tuples]
            tuples.append(line_count)
            (rx_id, rssi, lqi, angle, phaseA, phaseB, line_count) = tuples
            if rssi == 0 and lqi == 0:
                continue

            if rssi >= 0:
                raise ValueError('Invalid RSSI %d' % line_count)

            rssi -= 45
            data.append((rx_id, rssi, lqi, angle, phaseA, phaseB, line_count))





    df = pd.DataFrame(data)
    df.columns = ('rx_id', 'rssi', 'lqi', 'angle', 'phaseA', 'phaseB', 'line_count')
    base_fn = fn[:fn.rfind('.')]
    fnout = base_fn + '.csv'
    print('Saved', fnout)
    df.to_csv(fnout)
