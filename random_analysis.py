import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys


def load_dataset(fn):
    df = pd.DataFrame.from_csv(fn)
    return df


def plot_rx_pattern(df, phaseA_filter=None, phaseB_filter=None, figure=None):
    if phaseA_filter is not None:
        df = df[df.phaseA.isin(phaseA_filter)]
    if phaseB_filter is not None:
        df = df[df.phaseB.isin(phaseB_filter)]

    if figure is None:
        fig = plt.figure()
    else:
        fig = figure
    ax = fig.add_axes([0.02, 0.02, 0.98, 0.98], projection='polar')

    g = df.groupby(['angle', 'phase1', 'phase2'])
    rssi = g['rssi_avg'].mean()
    rssi = rssi.reset_index()
    rssi_g = rssi.groupby(['phase1', 'phase2'])
    for group, val in rssi_g:
        #print(group, val)
        a = np.array(val.angle)
        a = a / 200 * 2 * np.pi
        a = np.append(a, a[0])
        y = val.rssi_avg.values
        y = np.append(y, y[0])
        ax.plot(a, y)
    max_val = max(df.rssi_avg)
    min_val = min(df.rssi_avg)
    print (max_val,min_val)
    ax.set_rmax(max_val)
    ax.set_rmin(min_val)
    ax.set_yticks(range(-60,-100,-10))
    ax.set_yticklabels(map(str,range(-60,-100,-10)))

if __name__ == "__main__":
    fn = sys.argv[1]
    df = load_dataset(fn)
    plot_rx_pattern(df)
    plt.show()
