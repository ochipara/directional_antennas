'''
This script fixes some of the format issues in raw data
'''

import argparse

debug = 0

def main():
    global debug
    parser = argparse.ArgumentParser()
    parser.add_argument('file', nargs=1, type=str, help='Experiment to process raw data and format it.')
    args = parser.parse_args()
    arg_dict = vars(args)
    fn = arg_dict['file'][0]
    f = open(fn,'r')
    line = f.readline()
    fo = open('Mod'+fn,'w')
    while line:
        if line.strip('\r\n') == '':
            pass
        else:
            #Works for data starting with Test: or starting with digits
            if (line[0] == 'T' and line[1] != 'X') or line[0].isdigit():
                #could be blank line or remaining data
                line2 = f.readline()
                #concat them
                #print str(line.strip('\n\r'))+str(line2.strip('\n\r'))
                fo.write(line.strip('\n\r')+line2.strip('\n\r')+'\n')
            else:
                #pass
                #print str(line.strip('\n\r'))
                fo.write(line.strip('\n\r')+'\n')
        line = f.readline()
        #print line
    fo.close()
    f.close()

if __name__ == "__main__":
    main()
