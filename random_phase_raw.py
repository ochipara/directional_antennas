import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys

if __name__ == "__main__":
    fn = sys.argv[1]
    fn2 = sys.argv[2]
    fh2 = open(fn2,'w')
    for i in xrange(0,256,32):
        for j in xrange(0,256,32):
            fh = open(fn,'r')
            rmin = 100
            rmax = -1000
            rsum = 0
            rlen = 0
            line = fh.readline()
            trsum = 0
            while line:
                if line[0].isdigit():
                    line = line.strip('\s+')
                    data = line.split('\t')
                    if len(data) > 10:
                        if int(data[9]) == i and int(data[10]) == j:
                            if rmin > int(data[4]):
                                rmin = int(data[4])
                            if rmax < int(data[4]):
                                rmax = int(data[4])
                            rsum += int(data[4])
                            rlen += 1
                            trsum += int(data[4])
                            #print trsum
                            #fh2.write(str(data[4])+',')
                            if rlen % 50 == 0:
                                #print trsum
                                #print float(trsum/100)
                                fh2.write(str(float(trsum/50))+',')
                                trsum = 0
                    #print len(data)
                line = fh.readline()
            fh2.write('\n')
            print rmin,rmax,rsum,rlen
            fh2.write(str(i)+','+str(j)+','+str(rmin)+','+str(rmax)+','+str(float(rsum/rlen))+'\n')
            fh.close()
    fh2.close()
