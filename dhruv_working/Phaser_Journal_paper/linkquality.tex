% !TEX root = PHASER_Journal_paper.tex
\subsection{Link Quality Measurements}
\label{sec:link-quality-measurements}

\subsubsection{Link Quality}
In this section, we turn our attention to analyzing the properties of the links established between the \pn motes and the nodes comprising the testbed.
%We will start by studying the average case behavior and then turn our attention to analyzing the temporal variations.
Our analysis has two goals:
(1) demonstrate that varying phase configurations can significantly impact the quality of links and
(2) construct a predictive model that characterizes the relationship between the considered links.

Figure \ref{fig:linkquality-node0} characterizes the quality of the link established between \pn \texttt{A} and node 0.
The subfigures plot the PRR, RSSI, and LQI for different phase configurations.
The phase configuration is changed by varying the phase of each antenna in the range 0 -- 255 in increments of 64.
The figure clearly indicates that the quality of the links can be significantly changed by changing the configuration.
For example, the PRR can be changed from 0.5\% to 99\%.
Similarly, the RSSI varies in the range -83 dBm to -92 dBm while LQI in the range 75 to 105.

\begin{figure}[h]
\centering
\begin{subfigure}[t]{0.48\columnwidth}
\includegraphics[width=\columnwidth]{figures/links/testbed_rssi.pdf}
\caption{RSSI}
\label{fig:testbed-rssi}
\end{subfigure}
\begin{subfigure}[t]{0.48\columnwidth}
\includegraphics[width=\columnwidth]{figures/links/testbed_prr.pdf}
\caption{PRR}
\label{fig:testbed-prr}
\end{subfigure}
\caption{The impact of phase configuration changes on links from \pn \texttt{A} to testbed motes}
\label{fig:testbed-links}
\end{figure}

Figure \ref{fig:testbed-links} shows the RSSI and PRR of links from \pn \texttt{A} to the nodes in the testbed.
The link statistics of a node are computed by first considering the RSSI and PRR for each phase configuration.
The plotted values indicate the minimum, average, and maximum of these average values.
The results indicate that changes in the phase configuration can change the average RSSI by  6 -- 19 dBm (average 11 dBm).
This shows that can \pn can effectively the RSSI at a receiver within a wide range.
However, as shown in Figure \ref{fig:testbed-prr} the variability in PRR is smaller with a subset of links having high PRR irrespective of phase configuration.
The reason for this is that in order to observe packet losses, the PRR of a link must be reduced below a threshold value.
In some cases, this is not possible due to the proximity of the receiver to the \pn mote.
This highlights that in dense deployments phase changes must be combined with power control to increase spatial reuse.

\vspace{.3cm}
\noindent\textbf{\emph{Results 4:} 
Phase changes can change the receive signal strength between 6 -- 19 dBm.
However, the phase change may need to be combined with power control in order to maximize spatial reuse opportunities.}
\vspace{.3cm}

%Another challenge is find high quality links.
%We do this by evaluating different approaches.
%first we assess how stable a link quality is 
%we find the phase that provides the best link quality changes very fast, almost each round requires a different configuration
%
%we have evaluated the potential of using different link qulity estimator.
%first we evalaute an oracle performance.
%then we evaluate the best single configuration used retorspectively
%finally we consider a simple adaptive scheme.
%we search all the configurations. we pick the best lqi or rssi metric and stick with it until a packet drop.
%
%
%
%\begin{figure}[h]
%\centering
%\begin{subfigure}[t]{0.45\columnwidth}
%\includegraphics[width=\columnwidth]{figures/links/lqe_prr.pdf}
%\caption{PRR of selected configurations}
%\label{fig:lqe-prr}
%\end{subfigure}
%\begin{subfigure}[t]{0.45\columnwidth}
%\includegraphics[width=\columnwidth]{figures/links/lqe_rssi.pdf}
%\caption{RSSI of selected configurations}
%\label{fig:lqe-rssi}
%\end{subfigure}
%\caption{Link quality estimator}
%\end{figure}

\begin{figure}[th]
\centering
\begin{subfigure}[t]{0.492\columnwidth}
\includegraphics[width=\textwidth]{figures/phasematrix/node0}
\caption{Phase matrix of node 0}
\label{fig:pm0}
\end{subfigure}
%\hfill
\begin{subfigure}[t]{0.492\columnwidth}
\includegraphics[width=\textwidth]{figures/phasematrix/node1}
\caption{Phase matrix of node 1}
\label{fig:pm1}
\end{subfigure}
%\hfill
%\begin{subfigure}[t]{0.3\textwidth}
%\includegraphics[width=\textwidth]{figures/phasematrix/node7}
%\caption{Phase matrix of node 7}
%\label{fig:pm7}
%\end{subfigure}
\caption{Phase matrix for different nodes in the testbed for \pn \texttt{A}.}
\label{fig:phasematrix}
\end{figure}

\subsubsection{A Model for Phase-Shifting Antennas}
A key challenge to using directional antennas is that we must identify phase configurations that result in high RSSI or allow for spatial reuse.
In the following, we will show that it is possible to create a model that relates the RSSI of the links established from the \pn mote to the nodes in the testbed.
The model digests our understand of how \pn behaves in environments where there is line-of-sight.
%As detailed in Section \ref{sec:discussion}, we expect that this model can be used by protocols and simulators.

We start by observing that in Figure \ref{fig:linkquality-node0} there an interesting oscillation between low and high quality when link quality is measured using RSSI: the link's RSSI oscillates from -92 dBm to -82 dBm almost periodically.
This pattern is the results of how we iterate through the 64 measured antenna configurations in the figure.
In order to better understand the relationship between phases and RSSI, 
we replot the data as a pcolor plot where the rows and columns indicate the phases of antennas A and B while the color indicates the measured RSSI.
Figure \ref{fig:phasematrix}  plots the phase matrices for three nodes in the network.
The figure suggests that the patterns of the phase matrices are shifted and scaled version of the same underlying pattern.
If the hypothesis is correct, we should be able to reproduce the phase matrices of the other nodes from the base template.
Accordingly, assuming that the phase matrix is of size $N \times N$ and using the phase matrix of node 0 as a base template,
we can derive the phase matrix $PM_i$ of any other node $i$ in the network.
Specifically, the RSSI value of $PM_i$ for phase configuration $\phi_A$ and $\phi_B$ can be computed using:

\vspace{-.1cm}
\begin{displaymath}
PM_i[\phi_A, \phi_B] = o + \\
 s \times PM_{0}[(\phi_A+ \phi_A' )\textrm{\%} N, (\phi_B + \phi_B') \textrm{\%} N]
\end{displaymath}
\noindent where \% is the modulus operator. 
The model has four parameters.
The  $\phi_A'$ and $\phi_B'$ parameters shift the base template to better fit the target phase matrix.
 The offset $o$ and scale $s$ parameters correct for the fact that the nodes may have different RSSI levels as they are located at different distances relative to the receiver.

We wrote a program that fits the model in two steps.
First, we find the best phase shift for each antenna that minimizes the squared error between the entries in the considered phase matrix and those in the base template.
Since there at most 64 combinations of possible phase shifts, we exhaustively evaluate each one of them.
Second, we scale the values in the shifted phase matrix using least square optimizations to account that the phase matrix has a different range of RSSI values due to the nodes being located at different distances from \pn mote.

\begin{figure}[h]
\centering
\begin{subfigure}[t]{0.75\columnwidth}
\includegraphics[width=\columnwidth]{figures/phasematrix/adjusted_link.pdf}
\caption{Estimated link quality of node 1 when node 0 is used as a template}
\label{fig:adjustment}
\end{subfigure}
\begin{subfigure}[t]{0.75\columnwidth}
\includegraphics[width=\columnwidth]{figures/phasematrix/error_distr.pdf}
\caption{Distribution of absolute errors}
\label{fig:adjustment-errors}
\end{subfigure}
\caption{A model for phase-shifting directional antennas}
\end{figure}

%\begin{figure}[h]
%\includegraphics[width=\columnwidth]{figures/phasematrix/error_distr.pdf}
%\caption{Distribution of absolute errors due to model approximation}
%\label{fig:adjustment-errors}
%\end{figure}

Figure \ref{fig:adjustment} details the process of estimating the RSSI at node 1 based on that of node 0.
The first shows the original RSSI of nodes 0 and 1.
The RSSI of node 1 is estimated by first adjusting the phase of node 0's RSSI and then scaling it as shown in the figure. 
Figure \ref{fig:adjustment-errors} plots the errors observed when the link quality of nodes 1 -- 15 are estimated based on the link quality of node 0.
The median errors for all nodes are below 2 dBm suggesting that this approach is effective in accurately capturing the behavior of the links in the network.
We have evaluated the model using the five different motes.% and two antennas and obtained similar error rates.

\vspace{.3cm}
\noindent\textbf{\emph{Results 5:} 
The phase matrices measured at different receivers from a single \pn mote have the same underlying pattern.
Picking one the phase matrix of one of the nodes as a template, it is possible to reproduce the phase matrix of any other receiver by shifting and scaling the template with average absolute errors less than 2 dBm.
}
\vspace{.3cm}










