% !TEX root = PHASER_Journal_paper.tex
\section{Introduction}
\label{sec:introduction}

\sloppypar{
The wireless sensor network (WSN) community has extensively studied low-power communication when nodes are equipped with omnidirectional antennas.
However, in contrast to omnidirectional antennas, smart directional antennas can be controlled by software to transmit signals in a preferential direction to create multiple radiation patterns.
The diversity of radiation patterns can be exploited to selectively increase (or decrease) the signal at a receiver and improve spatial reuse.
This basic capability can be a foundation for building the next generation of low-power wireless stacks that can extend the reach of low-power wireless communication by improving throughput, energy-efficiency, reliability, and security.
}

The design of directional antennas for WSNs faces three challenges that are poorly addressed by existing antenna designs.
First, WSN nodes typically trade-off computation and memory resources in favor of energy efficient microcontrollers.
Accordingly, a directional antenna solution must employ only simple, lightweight signal processing techniques to keep energy consumption to a minimum.
Additionally, directional communication must introduce a minimal processing overhead given the paucity of computational resources.
Second, WSN nodes tend to be small and, as a result, their antennas must also be small.
Bulky antenna designs would significantly limit the extent to which they may be deployed.
Finally, directional antennas must be simple to manufacture.
Ideally, a directional antenna design should use off-the-self components that may be put together with minimal effort.

The performance of Wi-Fi networks can be significantly improved using directional communication techniques such as MIMO.
Unfortunately, Wi-Fi networks are less energy efficient than 802.15.4 radios at low data rates.
Incorporating MIMO techniques that require heavy signal processing of multiple radio signals will further reduce its efficiency in this operating regime.
A number of alternatives that are better suited for WSN applications have been proposed (see Section \ref{sec:relatedwork} for details).
A common approach to achieving directional communication are electronically switched parasitic element antennas.
This type of antennas has a central active element that is surrounded by parasitic elements.
A software driver controls the directionality of the radiated pattern by either grounding or isolating the parasitic elements.
When a parasitic element is grounded it reflects the radiated power; conversely, when the parasitic elements are isolated, they direct the radiated power.
This design was popularized by the SPIDA antenna whose link properties have been studied in \cite{Voigt2013}.
A limitation of SPIDA is that it performance is significantly affected by the antenna geometry and, in particular, the position of the parasitic elements.

In this paper, we present \pn~--- a mote prototype for low-power directional communication. 
\pn's design is modular and has three components: a low-power radio chip (e.g., CC2420), an RF signal processing chip (splitter and phase shifters), and off-the-self antennas (e.g., whip antennas).
Directional communication is achieved by splitting the output signal from the low-power radio chip.
A hardware chip is used to control the phase of the signals transmitted to the two antennas.
A software driver can be used to programmatically control the phase of the signals.
The net effect of controlling the phase of the signals is that they generate patterns of constructive and destructive interference as signals propagate.
If the resulting antenna patterns are diverse this can provide an approach to improving wireless communication performance.
In contrast to SPIDA, our design is simpler as it uses off-the-self components and does not require complex manufacturing.
It is our hope that by creating a simpler and open-source mote design, we will catalyze research on directional communication in WSNs.

In addition to the design of \pn, we present an extensive empirical study characterizing \pn's ability to support directional communication.
The presented results are obtained from five \pn prototypes which are equipped with either short or long whip antennas.
The short (66mm) and long (108mm) whip antennas differ in their gains.
The properties of the \pn motes are measured either outdoors or indoors using testbed composed of 16 TeslosB motes.
The results from the empirical study show that by changing the phase configuration, \pn can create significantly different radiation patterns both indoors and outdoors.
We introduce two metrics -- directional benefit and spatial reuse index -- to quantify the benefit of having multiple radiation patterns compared to a single one (as it is the case with omnidirectional antennas).
The directional benefit capture \pn's ability to improve the link quality to a receiver at a given orientation.
Our analysis shows that the link quality of a link can be improved by at least 13 dBm when \pn motes are equipped with the long whip antennas.
The directional benefit varies significantly depending on the location of the transmitter and receiver.
The average benefit varies little when a \pn mote is equipped with the same type of whip antennas.
However, long whip antennas provide improvement of 5 dBm over short whip antennas.

The spatial reuse index captures the potential opportunities for spatial reuse.
Considering two receivers, $R$ and $P$, we say that it is possible to transmit concurrently to $R$ and $P$ if the difference in the signal strength at $R$ and $P$ is exceeds a reuse threshold.
The spatial reuse index is defined as the fraction of orientations of the transmitter for which spatial reuse is possible.
Our analysis indicates that when the threshold is set to 6 dBm, the spatial reuse index is in the range 40\% -- 80\%.
The spatial reuse index decreases linearly with the increase in the reuse threshold.
This suggests that \pn is effective in creating spatial reuse opportunities.
As expected, the reuse opportunities depend on the orientation of the transmitter and location of the receiver.
The spatial reuse index was similar across the tested \pn prototypes and used antennas.
This shows that using different antennas results in different signal strength (as reflect in the differences in directional benefit), however, the diversity of these patterns is less affected by the gain of the antennas.

The last part of the empirical study focuses on exploring the link quality properties.
We observe that changing the phase configuration can effectively control the quality of the link as measured by receive signal indicator, link quality indicator, and packet reception rate.
More importantly, we show that in an environment where there is line of sight, the quality of the links from the same \pn mote have a common pattern.
We defined the phase matrix to be a matrix that the rows and columns indicate the phases with which signals are transmitted and the entries indicate the RSSI for a specific antenna configuration.
We show that if we use the phase matrix of a node as a template, we can reconstruct the phase matrices of any other node applying a simple transformation:
(1) shifting the phases to account for shifts in the phase matrix  and (2) scaling the obtained matrix to account for differences in RSSI to account that nodes have different distances to the receiver.
The median accuracy error varies between nodes, but is at most 2 dBm.
This model can be effectively exploited to find the appropriate phase patterns to both improve link quality and identify spatial reuse opportunity.
Estimating the phase shifts and scaling parameters is sufficient to characterize the observed RSSI at all nodes in line-of-sight conditions.

The remainder of the paper is organized as follows.
The design of \pn is presented in Section \ref{sec:hardware}.
We evaluate the performance of \pn empirically in Section \ref{sec:study}.
A discussion of the applicability of the presented results is included in Section \ref{sec:discussion}.
The related work is discussed in Section \ref{sec:relatedwork}.
Conclusions are presented in Section \ref{sec:conclusions}.

% The remainder of the paper ....
% ==== NOTES =====
% Mostly for the next paper...
% 
% ++ motivation of why directional antennas are beneficial. improved throughput, reduced energy, and improved security, security
% ++ there are different approaches to achieving directionality. 
% the most popular approach is to use a number of parasitic elements that can be turned on or off in order to preferentially attenuate.
% When grounded, they work as reflectors of ra- diated power, and when isolated they work as directors of radiated power. The SPIDA is equipped with six parasitic elements, yielding six possible �switches� to control the direction of transmission.

% to building directional antennas: using parasitic elements or phase shifting transmissions.
% 	
% 	-- compare and  contrast the two approaches
% 	-- the phase shifting approach leads to designs that are smaller and easier to deploy
% 	-- a potential limitation of phase shifting approaches is that it may have higher signal propagation losses within the circuit then the other approach. (correct?)
% 	-- perhaps higher variability between antenna properties in the parasitic case (correct?)
% 	-- which technique is MIMO using or is that very different?
% 	
% ++ the use of directional antennas in wireless sensor networks is in its infancy. only two in-depth empirical studies analyze the properties of directional antennas that use 
% parasitic elements. this paper complements this study by considering a different approach to implementing directional antennas. we analyze compare the relative merits of the two approaches. this information is particularly useful for adapting existing wireless stacks to support directional antennas
% 	
% 	-- we propose a set of metrics that are useful to compare the expected benefit of a directional antenna based on its radiation pattern.
% 	-- we characterize the link quality properties in both indoor and outdoor environments highlighting the profound effect multi-path has in indoor environments
% 	-- we characterize the stability of link quality over time in both indoors and outdoors. experiments highlight the need for continuous adaption of the antenna configuration patterns in order to achieve high link quality
% 	-- we propose a method for tracking the best antenna configuration over time 
% 	
% 