"""
This is a simple script to ingest the data from a log file.
It will go through and import all the relevant data.
The script will through a lot of errors if the data is not in the format that it expects.
Most of the issues are related to when a data line is split in two. For example,

Test:	4964	31	95	100
98	-38	106	0	53

will cause an error. You can fix it manually by putting:

Test:	4964	31	95	100	98	-38	106	0	53

After Dhruv fixes the serial code, we can tighten the requirements.
"""

import sys
import re
import pandas as pd
import numpy as np

rx_start_re = re.compile(r"Rx:\s+START")
rx_done_re = re.compile(r"Rx:\s+DONE")


def is_data(line, prev_line):
    count = 0
    for x in line:
        if x in ['0','1','2','3','4','5','6','7','8','9','-','\t',' ']:
            count = count + 1
    r = count / len(line)
    if r > .95:
        if prev_line is not None:
            if prev_line.find('TX_power:') >= 0: return False
            if prev_line.find('Ant_config:') >= 0: return False
            if prev_line.find('Angle_') >= 0: return False
            if prev_line.find('Send_') >= 0: return False

        return True
    else:
        return False


global prev_send_time
prev_send_time = None

def process_data_line(line, line_count, data, phases):
    global prev_send_time

    tuples = re.split('\s+', line)
    if len(tuples) == 9:
        tuples = [int(t) for t in tuples]
        tuples.append(line_count)
        (send_time, recv_time, sender_seq, recv_seq, rssi, lqi, power_level, angle, phase, line_count) = tuples
        if prev_send_time is not None and prev_send_time > send_time:
            raise ValueError('invalid send time at ln=%d' % line_count)
        prev_send_time = send_time

        if phase not in phases: #set([0,8,16,32,64,128,196,255]):
            raise ValueError('Invalid phase at ln=%d' % line_count)

        rssi = rssi - 45
        data.append((send_time, recv_time, sender_seq, recv_seq, rssi, lqi, power_level, angle, phase, line_count))
    else:
        raise Exception('failed to process data [%s] ln=%d' % (line, line_count))


def process_file(fn, phases):
    print('ingesting %s' % fn)
    with open(fn, 'r') as f:
        lines = f.readlines()

    line_count = 0
    data = []
    while line_count < len(lines):
        prev_line = lines[max(0, line_count - 1)]
        line = lines[line_count].strip()
        line_count += 1
        if len(line) == 0: continue
        if is_data(line, prev_line):
            try:
                process_data_line(line, line_count, data, phases)
            except Exception:
                orig_line = line

                try:
                    line = orig_line + lines[line_count]
                    line = line.strip()
                    process_data_line(line, line_count, data, phases)
                except Exception:
                    line = orig_line + '\t' + lines[line_count]
                    line = line.strip()
                    process_data_line(line, line_count, data, phases)
                line_count += 1

    return data


def add_round(data):
    rounds = []
    prev_seq = 0
    round = 0
    for row in data:
        (send_time, recv_time, sender_seq, recv_seq, rssi, lqi, power_level, angle, phase, line_count) = row
        if prev_seq >= sender_seq:
            round += 1

        rounds.append(round)
        prev_seq = sender_seq

    new_data = []
    for index in range(len(data)):
        new_row = list(data[index])
        new_row.append(rounds[index])
        new_data.append(new_row)

    return new_data


def check_send_time(df):
    x = df.send_time.values
    for index in range(1,len(x)):
        prev = x[index - 1]
        val = x[index]
        if prev > val:
            print('found inversion', index)


def check_round_phase_key(df):
    d = {}
    errors = []
    for index, row in df.iterrows():
        k = '%d-%d' % (row['exp_round'], row['phase'])
        if k in d:
            errors.append((k, d[k], row['line_count']))
        d[k] = row.line_count

    for error in errors:
        print('found duplicate key', error)


if __name__ == "__main__":
    fn = sys.argv[1]
    phases = set(np.arange(0, 256, 8))
    phases = phases.union([255])

    print('phases', sorted(phases))
    data = process_file(fn, phases)
    data = add_round(data)
    df = pd.DataFrame(data)
    df.columns = ('send_time', 'recv_time', 'sender_seq', 'recv_seq', 'rssi', 'lqi', 'power_level', 'angle', 'phase', 'line_count', 'exp_round')
    print(df.describe())

    print('running consistency checks...')
    check_send_time(df)
    check_round_phase_key(df)

    base_fn = fn[:fn.rfind('.')]
    fnout =base_fn + '.csv'
    print('Saved', fnout)
    df.to_csv(fnout)
