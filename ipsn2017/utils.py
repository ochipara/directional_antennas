import glob


def file_for_node(dir, node_id):
    files = glob.glob('%s/*NODE%d.csv' % (dir,node_id))
    if len(files) == 1:
        return files[0]
    else:
        raise Exception('File not found')
