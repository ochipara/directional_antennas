% !TEX root = ipsn-blind.tex
\subsection{Radiation Pattern Experiments}

\subsubsection{Radiation Patterns}

The initial experiment is designed to evaluate the performance of \pn in an environment with minimal external interference and multi-path effects.
The experiment was carried out on a rugby field where the turntable was placed in the middle of the field.
The \pn was powered using an external battery for the duration of the experiment.
The configuration of \pn was varied by holding the phase for antenna B constant and varying the phase of antenna A from 0 to 255 in increments of 32. 
%This results in phase changes in the range of $0\degree$ -- $360\degree$.
%Note that in order to avoid confusion between the phases and the orientation of \pn we will use numbers to between 0 -- 255 to describe phase configurations and angle measured in degrees to refer to orientations.

The radiation patterns observed during the experiment are shown in Figure \ref{fig:outdoors}.
The figure clearly shows that changing the phase configuration has a significant impact on the observed radiation patterns.
Most of the radiation patterns tend to be anisotropic displaying preferential propagation in a given direction.
For example, when the \pn is configured to $(192, 0)$ the radiation pattern resembles of horizontal ``figure 8''.
The main lobes of the antenna are pointed towards $0 \degree$ and $180 \degree$.
Conversely, the ``nulls'' of the antenna point are at $90 \degree$ and $270 \degree$, respectively.

The change of the phase configuration has a predictable behavior.
When the phase of antenna A, $\phi_A$ is 0, the main lobe of the antenna is pointed at $90 \degree$.
Increasing $\phi_A$ to 32 and later to 64 results in increasing the size of the lobe pointing towards $270\degree$ and shrinking of the lobe pointing at $90\degree$.
When $\phi_A = 64$, the radiation pattern looks like a vertical ``figure 8''.
Increasing $\phi_A$ to 96, 128, and 160 shows a slow transition from having lobes pointing to $90\degree$ and $270\degree$ to lobes pointing to $0\degree$ and $180\degree$.
When $\phi_A$ is 192, the radiation pattern looks like a horizontal ``figure 8''.

The above sequence of figures clearly shows that controlling the phases of the signals we can create different radiation patterns.
The key is to appropriately select the radio pattern depending on your communication goal.
For example, in the case you want to communicate with that has an orientation of $90\degree$ you may want to use configuration $(64,0)$.
Additionally, this configuration opens opportunities for spatial reuse.
For example, other nodes that are located at the ``nulls'' of the radiation pattern (i.e., $0 \degree$ or $180 \degree$) may be used as other transmissions.
Alternatively, if you consider the security concerns due to eavesdropping, a transmission using this pattern would leak the minimal amount of information to the same positions.

We have repeated the same experiment indoors when the data was collected using one of the motes in the testbed.
Figure \ref{fig:indoors} plots the radiation pattern that we observed indoors.
Similar to the outdoor setting, the Figure \ref{fig:indoors} clearly shows that changing the \pn configuration can significantly impact the radiation pattern.
However, while there are some similarities between the observed patterns, there are also clear differences.
These differences may be attributed to a combination of the slightly different positioning (the \pn mote and the receiver are not at the same height as it was the case in the indoor experiments) as well as to multi-path effects.
The differences suggest that the observed radiation patterns depend significantly depend on the location of the nodes and the environment, which warrens a closer examination of these factors.
In the following, we will explore these factors by identifying the variability in directional benefit and spatial reuse index observed at different receivers.

\begin{figure}[h]
\begin{subfigure}[t]{0.45\columnwidth}
\includegraphics[width=\columnwidth]{figures/tx_motea_antm_all}
\caption{Radiation patterns}
\label{fig:tx-radiation-pattern}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.45\columnwidth}
\includegraphics[width=\columnwidth]{figures/directional_benefit/directional_benefit_moteA-antennaM-node0}
\caption{Directional benefit}
\label{fig:directional-benefit-for-node1}
\end{subfigure}
\caption{Patterns and directional benefit of \texttt{A+M} as measured by node 1.}
\end{figure}

\begin{figure*}
\centering
\begin{subfigure}[t]{0.3\textwidth}
\includegraphics[width=\textwidth]{figures/directional_benefit/directional_benefit_bar}
\caption{Directional benefit per node}
\label{fig:directional-benefit-per-node}
\end{subfigure}
\begin{subfigure}[t]{0.3\textwidth}
\includegraphics[width=\textwidth]{figures/directional_benefit/directional_benefit_antM}
\caption{Long whip antennas}
\label{fig:directional-benefit-long-whip}
\end{subfigure}
\begin{subfigure}[t]{0.3\textwidth}
\includegraphics[width=\textwidth]{figures/directional_benefit/directional_benefit_antS}
\caption{Short whip antennas}
\label{fig:directional-benefit-short-whip}
\end{subfigure}
\caption{The directional benefit depends on the location of the receiver as well as the type of antennas with which \pn motes are equipped.}
\label{fig:directional-benefit}
\end{figure*}

\vspace{.3cm}
\noindent\textbf{\emph{Results 1:} Changing the phase configuration creates diverse radiation patterns in both outdoors and indoors environments.
Each fixed configuration creates a radiation pattern that is anisotropic.}

\subsubsection{Directional Benefit}
In this section, we evaluate the directional benefit provided by \pn.
In this set of experiments, the configuration of \pn is changed by varying the phases $\phi_A$ and $\phi_B$ in range 0 -- 255 in increments of 32.
Figure \ref{fig:tx-radiation-pattern} plots the radiation patterns for mote \texttt{A+M} as measured by a node 1.
The node's directional benefit is computed using equation \ref{eq:directional-benefit} and plotted in Figure \ref{fig:directional-benefit-for-node1}.
Essentially, the directional benefit is the difference between the outer hull (i.e., maximum RSSI across all configuration) and the inner hull (i.e., minimum RSSI across all configuration) of Figure \ref{fig:tx-radiation-pattern}.

The directional benefit of node 1 varies between 13 -- 32 dBm depending on the orientation of the \pn mote.
Therefore, a minimum increase of 13 dBm can be observed over the worst-case for any orientation of the transmitter.
On average, the directional benefit is 24 dBm, which is an increase in the receive signal strength of over two orders of magnitude. 
These statistics clearly show the benefit of using directional antennas.
In the case of a static antenna, a receiver would have a single anisotropic radiation pattern that could lead to some nodes having low link quality.
In contrast, \pn can increase the RSSI by at least 13 dBm and up to 24 dBm on average.
We will show that this predicted benefit is materialized in that \pn can transform low quality link into a high quality link as discussed in Section \ref{sec:link-quality-measurements}.
Figure \ref{fig:directional-benefit-for-node1} shows that the directional benefit is anisotropic.
This indicates that the benefit that may be provided by \pn depends on the orientation of the \pn mote.

Figure \ref{fig:directional-benefit-per-node} plots the average directional benefit computed for at each node in the testbed when different \pn prototypes are used.
The figure indicates that different nodes in the testbed have different average directional benefits ranging from 10 dBm to 24 dBm.
We have also evaluated the directional benefit using different \pn mote.
The figure indicates that the nodes that have the largest directional benefit change depending on the \pn mote that is used.
Given that the devices were assembled by hand and are subject to variations in soldering quality these variations in radiation patterns are expected.
In order to evaluate the variability across the different \pn prototype on more detail, we plot the distribution of average directional benefits using a box plot.
Figure \ref{fig:directional-benefit-long-whip} shows the performance of the \pn prototypes when the long whip antennas are used.
The \pn prototypes that use the same type of antenna had similar performance in terms of directional benefit.
This shows that even though there is variability in different receivers, the performance of a \pn mote averaged over the testbed mote is similar.
Comparing Figure \ref{fig:directional-benefit-long-whip}  and Figure \ref{fig:directional-benefit-short-whip}, we can observe that the long whip antennas increase the directional benefit by 5 dBm over short whip antennas.
We have validated that this difference can be attributed to the fat that the long whip antennas provide higher gain the short whip antennas resulting in higher RSSI values measured by TelosB motes.

\vspace{.3cm}
\noindent\textbf{\emph{Results 2:} 
The directional benefit depends on the orientation of the \pn mote, location of the receiver, and hardware.
While the specific benefit observed by a receiver depends on its location, there is only a small variation in the distribution of directional benefits over the entire testbed when \pn motes use the same type of antenna.
However, using long whip antennas provides an additional directional benefit as large as 5 dBm over short whip antennas.}

\subsubsection{Spatial Reuse}

A key benefit of directional antennas is that they may allow a greater degree of spatial reuse than traditional omnidirectional antennas.
Figure \ref{fig:interference-benefit-fixed} plots the radiation pattern for the phase configuration that maximizes $RSSI(c, \gamma_R) - RSSI(c, \gamma_P)$, when the target and reuse nodes are placed at $\gamma_R$ and $\gamma_P$, respectively.
%As expected, we may take advantage of the asymmetry of the radiation pattern in order to foster spatial reuse.
In our example, there is a clear dip in RSSI when $\gamma_P=90\degree$ while there is a high RSSI at $\gamma_P = 0\degree$.
This example illustrate how \pn may take advantage of the anisotropy of radiation patterns to foster spatial reuse.
We conjecture that if we were to use antennas that have higher anisotropy, \pn may achieve higher spatial reuse.

\begin{figure}[h!]
\centering
\begin{subfigure}[t]{0.48\columnwidth}
\includegraphics[width=\columnwidth]{figures/spatialreuse/sri_recv0deg_pattern.pdf}\caption{Radiation pattern resulting in the highest SRI when receivers $R$ and $R$ are located at  $\gamma_R = 0\degree$ and $\gamma_P = 90\degree$.}
\label{fig:interference-benefit-fixed}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.48\columnwidth}
\includegraphics[width=\columnwidth]{figures/spatialreuse/sri_recv0deg.pdf}
\caption{SRI when the receiver $R$ is at $\gamma_R = 0\degree$ and the receiver $P$ is at $\gamma_P \in [0\degree,360\degree]$ }
\label{fig:interference-benefit-receiver}
\end{subfigure}
\caption{Spatial reuse index}
\label{fig:spatialresueindex}
\end{figure} 

Figure \ref{fig:interference-benefit-receiver} plots $RSSI(c, \gamma_R) - RSSI(c, \gamma_P)$ when $\gamma_R = 0$ and $\gamma_P$ is varied in the range $0\degree$ -- $360\degree$.
The figure shows that when $\gamma_P \simeq \gamma_R$, it is impossible to have spatial reuse.
This is because if nodes $P$ and $R$ have a similar distance and orientation to the transmitter, it is difficult to find a radiation pattern that can simultaneously increase the signal strength $P$ and decrease it at $R$.
%The region of angles where spatial reuse is not possible depends on the beamwidth of the antenna, which in the case of the whip antennas used in this study can be as large as 45 degrees.
Spatial reuse is also not possible when $\gamma_P \simeq 240\degree$.
This is because of the symmetry of the created radiation patterns.

In order to compare the impact of the location of the receiver on spatial reuse, we computed the spatial reuse index for all the nodes in the testbed for different \pn combinations.
We have used a threshold of 6 dBm as previous study on interference suggest that 6 dBm are sufficient to take advantage of the capture effect.
Figure \ref{fig:spatialreuse-per-node} plots the spatial reuse index of different nodes.
We remind the reader that the spatial reuse index has values in the range 0\% -- 100\% indicating the fraction of angles for which we expect that spatial reuse is possible.
For \texttt{A+M}, the spatial reuse index varies between 43\% -- 87\% covering a large fraction of possible directions.
Figure \ref{fig:spatialreuse-threshold} plots the impact of increasing the signal threshold from 4 dBm to 26 dBm.
As expected, as we increase the threshold, a smaller fraction of angles allow for spatial reuse.
The spatial reuse index decreases linearly as the signal threshold is increased.
This behavior is consistent across all the nodes in the testbed.
These results indicate that \pn can create significant opportunities for spatial reuse.

Figure \ref{fig:spatialreuse-hardare} evaluates the impact of different \pn hardware on the distribution of spatial reuse indices measured over the entire testbed.
The figure shows that either changing the mote or the antenna type has a limited impact of the distribution of spatial reuse index.
This can be explained by the fact that antenna patterns are equally diverse regardless of using a short or a long whip antenna.
In other words, even though the antennas provide different gains the underlying radiation patterns have similar characteristics (after all both antennas are monopoles).

\begin{figure}[h]
\centering
\begin{subfigure}[t]{0.7\columnwidth}
\includegraphics[width=\columnwidth]{figures/spatialreuse/spatialreuse_nodes_antM.pdf}
\caption{Spatial reuse index measured by testbed nodes}
\label{fig:spatialreuse-per-node}
\end{subfigure}
\begin{subfigure}[t]{0.7\columnwidth}
\includegraphics[width=\columnwidth]{figures/spatialreuse/spatialreuse_threshold_antM.pdf}
\caption{Spatial reuse index of \texttt{A+L} measured by nodes 0, 4, 8, and 12}
\label{fig:spatialreuse-threshold}
\end{subfigure}
\caption{Spatial reuse index varies with receiver location and decreases linearly with the signal threshold}
\end{figure}

\begin{figure}[h]
\centering
\begin{subfigure}[t]{0.7\columnwidth}
\includegraphics[width=\textwidth]{figures/spatialreuse/spatialreuse_antM.pdf}
\caption{Spatial reuse for long whip antennas}
\label{fig:spatialreuse-long}
\end{subfigure}
\begin{subfigure}[t]{0.7\columnwidth}
\includegraphics[width=\textwidth]{figures/spatialreuse/spatialreuse_antS.pdf}
\caption{Spatial reuse for short whip antennas}
\label{fig:spatialreuse-short}
\end{subfigure}
\caption{Impact of different \pn hardware on spatial reuse index}
\label{fig:spatialreuse-hardare}
\end{figure}


\begin{figure*}
\centering
\begin{subfigure}[t]{0.3\textwidth}
\includegraphics[width=\textwidth]{figures/links/prr_moteA_pwr7_ch11_node0}
\caption{PRR of node 0}
\label{fig:prr0}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.3\textwidth}
\includegraphics[width=\textwidth]{figures/links/rssi_moteA_pwr7_ch11_node0}
\caption{RSSI of node 0}
\label{fig:rssi0}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.3\textwidth}
\includegraphics[width=\textwidth]{figures/links/lqi_moteA_pwr7_ch11_node0}
\caption{LQI of node 0}
\label{fig:lqi0}
\end{subfigure}
\caption{The quality of the link from \texttt{A+L} measured at node 0 using PRR, LQI, and RSSI}
\label{fig:linkquality-node0}
\end{figure*}

\vspace{.3cm}
\noindent\textbf{\emph{Results 3:} 
The spatial reuse opportunities depend on the orientation of the \pn mote and location of receiver.
While the spatial reuse of a specific node is affected by the \pn hardware, the \pn hardware has a small impact on the distribution of spatial reuse index values over the entire testbed.
This suggest that the radiation patterns that are obtained have similar degree of anisotropy.}






