import numpy as np
import pandas as pd
from temporalanalysis.temporal_analysis_ab import compute_avg_behavior, plot_avg_behavior, load_dataset, compute_phase_matrix, plot_phase_matrix, plot_avg_rssi,testbed_phase_matrix
from scipy.optimize import leastsq
import matplotlib.pyplot as plt


def phase_error(x, PM0, PM1):
    d = 0
    #print(x)
    for r in range(8):
        for c in range(8):
            dx = PM1[(r + x[0]) % 8, (c + x[1]) % 8] - PM0[r, c]
            d += dx * dx
    return d


def scale_matrix(x, PM0, PM1):
    d = np.zeros(PM0.shape)
    for r in range(8):
        for c in range(8):
            d[r,c] = x[0] + x[1] * PM1[r, c] - PM0[r, c]

    dd = d.ravel()
    return dd


def align_matrix(PMbase, PM):
    dbest = None
    phase = None
    for p1 in range(8):
        for p2 in range(8):
            d = phase_error([p1, p2], PMbase, PM)
            if dbest is None or dbest > d:
                dbest = d
                phase = (p1, p2)
    return phase


def apply_phase(x, Pin):
    Pout = np.zeros(Pin.shape)
    for r in range(8):
        for c in range(8):
            Pout[r, c] = Pin[(r + x[0]) % 8, (c + x[1]) % 8]

    return Pout


def traverse_matrix(phase):
    C = np.zeros((8,8))
    index = 0
    for r in range(8):
        for c in range(8):
            C[r, c] = index
            index += 1

    x = []
    for r in range(8):
        for c in range(8):
            x.append(int(C[(r + phase[0]) % 8, (c + phase[1]) % 8]))

    return x

if __name__ == "__main__":
    #PM = testbed_phase_matrix('/Users/ochipara/Working/DirectionalAntennas/data/testbed/tx_temporal/Ch11_Pwr7_MoteA', '2016-10-05_NODE')
    # PM = testbed_phase_matrix('/Users/ochipara/Working/DirectionalAntennas/data/testbed/tx_temporal/Ch11_Pwr7_MoteD','2016-10-07_NODE')


    df0 = load_dataset(
        '/Users/ochipara/Working/DirectionalAntennas/data/testbed/tx_temporal/Ch11_Pwr7_MoteA/2016-10-05_NODE%d.csv' % 0)
    df1 = load_dataset(
        '/Users/ochipara/Working/DirectionalAntennas/data/testbed/tx_temporal/Ch11_Pwr7_MoteA/2016-10-05_NODE%d.csv' % 1)
    PM0 = compute_phase_matrix(df0)
    PM1 = compute_phase_matrix(df1)

    phase01 = align_matrix(PM1, PM0)
    PM0_phased = apply_phase(phase01, PM0)
    r = leastsq(scale_matrix, [np.mean(PM0), 1], args=(PM1, PM0_phased))[0]

    print(r)
    f, rssi_avg1 = plot_avg_rssi(df1)
    f, rssi_avg0 = plot_avg_rssi(df0, figure=f, mf='b')
    ix = traverse_matrix(phase01)

    y = np.array([rssi_avg0.values[x] for x in ix])
    plt.plot(np.arange(0, 64), y, 'go-')

    y2 = r[0] + r[1] * y
    plt.plot(np.arange(0, 64), y2, 'ko-')
    plt.legend(['node 1', 'node 0', 'node 0 (phase)', 'node 0 (phase+scaling)'])
    plt.ylim(-92, -55)

    plt.show()

    #     plt.figure()
    #     plot_phase_matrix(PM[node])
    #     plt.savefig('figures/phase_matrix/node%d.pdf' % node)
    #
    # for node in range(1, 16):
    #     phase = align_matrix(PM[0], PM[node])
    #     PM[node] = apply_phase(phase, PM[node])
    #     plt.figure()
    #     plot_phase_matrix(PM[node])
    #     plt.savefig('figures/phase_matrix/adjusted_node%d.pdf' % node)
    #
    #     plt.figure()
    #     D = abs(PM[node] - PM[0])
    #     print(np.mean(D))
    #     plot_phase_matrix(D)
    #     plt.savefig('figures/phase_matrix/error_%d.pdf' % node)

