"""
Computes the benefit of a directional antenna
"""
from ingest import load_experiment
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def compute_directional_benefit(df, power_level):
    df2 = df[df.power == power_level]

    g = df2.groupby(['angle'])
    r = {}
    for angle, value in g:
        max_val = value.rssi_avg.max()
        min_val = value.rssi_avg.min()
        r[angle] = max_val - min_val

    x = sorted(list(r.keys()))
    y = [r[k] for k in x]
    B = pd.DataFrame({'angle': x, 'benefit': y})
    return B


def plot_directional_benefit(benefit_df, axis=None):
    if axis is None:
        fig = plt.figure(figsize=(8, 8))
        ax = fig.add_axes([0.1, 0.1, 0.8, 0.8], projection='polar')
    else:
        ax = axis

    x = benefit_df.angle * 2 * np.pi / 200
    x = x.values
    y = benefit_df.benefit.values
    print(x)
    x = np.append(x, 2 * np.pi)
    y = np.append(y, y[0])
    ax.plot(x, y)
    #ax.set_rmin(min(y))
    #ax.set_rmax(max(y))



if __name__ == "__main__":
    df = pd.DataFrame.from_csv('/Users/ochipara/Working/DirectionalAntennas/data/testbed/experiment1/2016-09-03_NODE8_1.csv')
    dirbenefit = compute_directional_benefit(df, 31)
    plot_directional_benefit(dirbenefit)

    # motea_antm_df = {}
    # for node in range(16):
    #     motea_antm_df[node] = pd.DataFrame.from_csv('/Users/ochipara/Working/DirectionalAntennas/data/testbed/antennapatterns/MoteC_AntennaM/2016-09-24_NODE%d.csv' % node)
    #
    #     dirbenefit = compute_directional_benefit(motea_antm_df[node], 31)
    #     plot_directional_benefit(dirbenefit)
    plt.show()



# print(B.describe())
# 	plot_benefit(B)
# 	plt.show()
