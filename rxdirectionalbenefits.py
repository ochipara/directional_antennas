import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys


def load_dataset(fn):
    df = pd.DataFrame.from_csv(fn)
    return df


def plot_rx_pattern(df, phaseA_filter=None, phaseB_filter=None, figure=None):
    if phaseA_filter is not None:
        df = df[df.phaseA.isin(phaseA_filter)]
    if phaseB_filter is not None:
        df = df[df.phaseB.isin(phaseB_filter)]

    if figure is None:
        fig = plt.figure()
    else:
        fig = figure
    ax = fig.add_axes([0.02, 0.02, 0.98, 0.98], projection='polar')
    g = df.groupby(['angle', 'phaseA', 'phaseB'])
    rssi = g['rssi'].mean()
    rssi = rssi.reset_index()
    rssi_g = rssi.groupby(['phaseA', 'phaseB'])
    max_val = max(df.rssi)
    #min_val = min(df.rssi)
    for group, val in rssi_g:
        print(group, val)
        min_val = min(df.rssi)
        a = np.array(val.angle)
        a = a / 200 * 2 * np.pi
        a = np.append(a, a[0])
        y = val.rssi.values - min_val
        y = np.append(y, y[0])
        #print (a,y)
        ax.plot(a, y)
    print (max_val,min_val)
    ax.set_rmax(max_val)
    ax.set_rmin(min_val)
    ax.set_yticks(range(0,30,5))
    ax.set_yticklabels(map(str,range(0,30,5)))

if __name__ == "__main__":
    #fn = '/Users/ochipara/Working/DirectionalAntennas/data/testbed/rx_moteA_ch26_pwr31/2016-10-06_NODE0.csv'
    fn = sys.argv[1]
    #fn = '/Users/ochipara/Working/DirectionalAntennas/data/testbed/rx_pattern/rx_moteB_ch26_pwr31/2016-10-08_NODE0.csv'
    df = load_dataset(fn)
    plot_rx_pattern(df)
    plt.show()
