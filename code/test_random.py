import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import pickle
import os
import sys


def load_dataset(fn):
    """
    Loads the dataset and removes the rssi values that are invalid.
    :param fn:
    :return:
    """
    df = pd.DataFrame.from_csv(fn)
    df = df[df.rssi < 0]
    return df

fn = sys.argv[1]
#fn = '/Users/ochipara/Working/DirectionalAntennas/data/testbed/tx_temporal/AB_Power7_Channel11/2016-09-25_NODE14.csv'
df = load_dataset(fn)
df2 = df[['phase1', 'phase2', 'round_index']]
df2 = df2.groupby(['phase1', 'phase2'])
for index, vals in df2:
    #print(index, vals)
    plt.figure()
    plt.hist(vals.round_index.values, 64)
    plt.title('%s,%s' % index)

plt.show()
