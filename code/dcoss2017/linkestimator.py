from temporalanalysis.temporal_analysis_ab import testbed_phase_matrix
import numpy as np


from temporalanalysis.temporal_analysis_ab import compute_avg_behavior, plot_avg_behavior, load_dataset, compute_phase_matrix, plot_phase_matrix, plot_avg_rssi,testbed_phase_matrix


def best_rssi_sequence(df):
    g = df.groupby(['round'])
    rssi_vals = []
    rssi_configs = []
    for round, values in g:
        rssi = np.max(values['rssi'])

        ix = values['rssi'].argmax()
        rssi_vals.append(rssi)
        rssi_configs.append((values.phase1[ix], values.phase2[ix]))

    return rssi_vals, rssi_configs


def compute_stability(rssi_configs):
    prev = None
    stable = 1
    x = []
    for (phase1, phase2) in rssi_configs:
        if prev is not None:
            if prev[0] == phase1 and prev[1] == phase2:
                stable += 1
            else:
                x.append(stable)
                stable = 1

        prev = (phase1, phase2)
    return x


def best_single_configuration(df, metric='rssi'):
    g = df.groupby(['phase1', 'phase2'])
    mrssi = g[metric].mean()

    (phase1, phase2) = mrssi.argmax()
    s = df[(df.phase1 == phase1) & (df.phase2 == phase1)]
    resets = np.max(s['round']) - len(np.unique(s['round']))
    return g['rssi'].mean().max(), mrssi.argmax(), resets

def best_single_prr_configuration(df):
    g = df.groupby(['phase1', 'phase2'])
    mrssi = g['round'].count()

    (phase1, phase2) = mrssi.argmax()
    s = df[(df.phase1 == phase1) & (df.phase2 == phase1)]
    resets = np.max(s['round']) - len(np.unique(s['round']))
    return g['rssi'].mean(), mrssi.argmax(), resets

def traverse_and_stick(df, metric='rssi'):
    g = df.groupby(['round'])

    current_rssi = None
    current_config = None
    resets = 0
    rssi_vals = []
    rssi_config = []
    for round, values in g:
        if current_rssi is None:
            current_rssi = np.max(values.rssi)
            ix = values[metric].argmax()
            current_config = (values.phase1[ix], values.phase2[ix])
        else:
            current_rssi = values.rssi[(values.phase1 == current_config[0]) & (values.phase2 == current_config[1])]
            if len(current_rssi) == 0:
                current_rssi = None
                current_config = None
                resets += 1
            #print(current_rssi)

        if current_config is not None:
            rssi_vals.append(current_rssi)
            rssi_config.append(current_config)

    return rssi_vals, rssi_config, resets

if __name__ == "__main__":
    for node in range(16):
        fn = '/Users/ochipara/Working/DirectionalAntennas/data/testbed/tx_temporal/Ch11_Pwr7_MoteA/2016-10-05_NODE%d.csv' % node
        df = load_dataset(fn)

        rssi_vals, rssi_configs = best_rssi_sequence(df)
        print('best-rssi-seq', node, np.mean(rssi_vals), 0)

        sb_vals, sb_configs,resets = best_single_configuration(df)
        print('single-rssi-seq', node, np.mean(sb_vals), resets)

        sb_vals, sb_configs, resets = best_single_configuration(df, metric='lqi')
        print('single-lqi-seq', node, np.mean(sb_vals), resets)

        rssi_vals, rssi_config, resets = traverse_and_stick(df)
        print('fs-rssi', node, np.mean(rssi_vals), resets)

        rssi_vals, rssi_config, resets = traverse_and_stick(df, metric='lqi')
        print('fs-lqi', node, np.mean(rssi_vals), resets)



