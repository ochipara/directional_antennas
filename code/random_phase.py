import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys

if __name__ == "__main__":
    fn = sys.argv[1]
    fn2 = sys.argv[2]
    fh2 = open(fn2,'w')
    for i in xrange(0,256,32):
        for j in xrange(0,256,32):
            fh = open(fn,'r')
            rmin = 100
            rmax = -1000
            rsum = 0
            rlen = 0
            line = fh.readline()
            while line:
                if line[0].isdigit():
                    line = line.strip('\s+')
                    data = line.split(',')
                    if int(data[10]) == i and int(data[11]) == j:
                        if rmin > int(data[5]):
                            rmin = int(data[5])
                        if rmax < int(data[5]):
                            rmax = int(data[5])
                        rsum += int(data[5])
                        rlen += 1
                line = fh.readline()
            print rmin,rmax,rsum,rlen
            fh2.write(str(i)+','+str(j)+','+str(rmin)+','+str(rmax)+','+str(float(rsum/rlen))+'\n')
            fh.close()
    fh2.close()
