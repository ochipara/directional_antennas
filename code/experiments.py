import numpy as np
import pandas as pd


class Experiment:
    """
    This class holds the data for an experiment.
    """

    def __init__(self, platform):
        self.platform = platform
        self.send_delay = None
        self.send_count = None
        self.angle_step = None
        self.angle_count = None
        self.data = []
        self.data_header = ['seq', 'power', 'angle', 'phase', 'prr', 'rssi_avg', 'lqi_avg', 'rssi_std', 'lqi_std']
        self.power_level = []
        self.angle = []
        self.phase = []
        self.prr = []
        self.rssi_avg = []
        self.lqi_avg = []

    def add_data(self, seq, power_level, phy_angle, antenna_phase, prr, rssi_avg, lqi_avg, rssi_std, lqi_std):
        self.power_level.append(int(power_level))
        self.angle.append(int(np.around(float(phy_angle) / 200.0 * 360.0)))
        self.phase.append(int(antenna_phase))
        self.prr.append(int(prr))
        self.rssi_avg.append(float(rssi_avg))
        self.lqi_avg.append(float(lqi_avg))

        self.data.append((seq, power_level, phy_angle, antenna_phase, prr, rssi_avg, lqi_avg, rssi_std, lqi_std))

    def get_dataframe(self):
        return pd.DataFrame({'phase': self.phase,
                             'angle': self.angle,
                             'power_level': self.power_level,
                             'rssi': self.rssi_avg})

    def unique_phases(self):
        return np.unique(self.phase)

    def filter_data(self, attr, filter_power_level=None):
        """
        TODO: add more things to filter on
        """
        tofilter = getattr(self, attr)
        r = []

        for index in len(tofilter):
            include = True
            if filter_power_level is not None:
                power = self.power_level[index]
                include = include and (power in filter_power_level)

            if include:
                r.append(tofilter[index])

        return r

    def print_header(self):
        return 'platform=%s send_delay=%d send_count=%d' % (self.platform, self.send_delay, self.send_count)

    def save_csv(self, name):
        with open(name, 'w') as f:
            # write the metadata for the file
            f.write('send_delay,%d\n' % self.send_delay)
            f.write('send_count,%d\n' % self.send_count)
            f.write('angle_step,%d\n' % self.angle_step)
            f.write('angle_count,%d\n' % self.angle_count)
            f.write('\n\n')

            # write the data
            f.write(','.join(self.data_header) + '\n')
            for item in self.data:
                line = ','.join(item)
                f.write(line + '\n')

    def __str__(self):
        return self.print_header() + ' data=%d' % len(self.data)


class Experiment2:
    def __init__(self, platform):
        self.platform = platform
        self.send_delay = None
        self.send_count = None
        self.angle_step = None
        self.angle_count = None
        self.data = []
        self.data_header = ['recv_time', 'sender_seq', 'rssi', 'lqi', 'power_level', 'angle', 'phase']
        self.power_level = []
        self.angle = []
        self.phase = []
        self.recv_time = []
        self.rssi = []
        self.lqi = []
        self.sender_seq = []

    def add_data(self, send_time, recv_time, sender_seq, recv_seq, rssi, lqi, power_level, angle, phase):
        self.recv_time.append(int(recv_time))
        self.recv_time.append(int(sender_seq))
        self.power_level.append(int(power_level))
        self.angle.append(int(np.around(float(angle) / 200.0 * 360.0)))
        self.phase.append(int(phase))
        self.rssi.append(float(rssi))
        self.lqi.append(float(lqi))

        self.data.append((recv_time, sender_seq, rssi, lqi, power_level, angle, phase))

    def get_dataframe(self):
        return pd.DataFrame({'phase': self.phase,
                             'angle': self.angle,
                             'power_level': self.power_level,
                             'rssi': self.rssi})

    def print_header(self):
        return 'platform=%s send_delay=%d send_count=%d' % (self.platform, self.send_delay, self.send_count)

    def save_csv(self, name):
        with open(name, 'w') as f:
            # write the metadata for the file
            f.write('version,2\n')
            f.write('send_delay,%d\n' % self.send_delay)
            f.write('send_count,%d\n' % self.send_count)
            f.write('angle_step,%d\n' % self.angle_step)
            f.write('angle_count,%d\n' % self.angle_count)
            f.write('\n\n')

            # write the data
            f.write(','.join(self.data_header) + '\n')
            for item in self.data:
                line = ','.join(item)
                f.write(line + '\n')

    def __str__(self):
        return self.print_header() + ' data=%d' % len(self.data)


def load_experiment(fn):
    with open(fn, 'r') as f:
        lines = f.readlines()

    if lines[0].strip() == 'version,2':
        experiment = Experiment2('phaser')
        metadata = True
        for line in lines:
            line = line.strip()
            if len(line) > 0:
                if metadata:
                    (tag, value) = line.split(',')
                    setattr(experiment, tag, int(value))
                else:
                    if line.startswith('recv_time'):
                        pass
                    else:
                        (recv_time, sender_seq, rssi, lqi, power_level, angle, phase) = line.split(',')
                        experiment.add_data(recv_time, sender_seq, rssi, lqi, power_level, angle, phase)
            else:
                metadata = False
    else:
        experiment = Experiment('phaser')
        metadata = True
        for line in lines:
            line = line.strip()
            if len(line) > 0:
                if metadata:
                    (tag, value) = line.split(',')
                    setattr(experiment, tag, int(value))
                else:
                    if line.startswith('seq'):
                        pass
                    else:
                        (
                        seq, power_level, phy_angle, antenna_phase, prr, rssi_avg, lqi_avg, rssi_std, lqi_std) = line.split(
                            ',')
                        experiment.add_data(seq, power_level, phy_angle, antenna_phase, prr, rssi_avg, lqi_avg, rssi_std,
                                            lqi_std)
            else:
                metadata = False

    return experiment