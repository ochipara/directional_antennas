import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import sys

def load_dataset(fn):
    phaseA = []
    phaseB = []
    Rssi = []
    fh = open(fn,'r')
    line = fh.readline()
    while line:
        #print (line)
        line = line.strip('\n')
        line = line.split(',')
        if len(line) > 4:
            phaseA.append(line[0])
            phaseB.append(line[1])
            Rssi.append(line[4])
        line = fh.readline()
    fh.close()
    return phaseA, phaseB, Rssi

def plot_rx_pattern(phaseA,phaseB,Rssi):
    plt.plot(Rssi)
    #label = []
    #for x, y in phaseA, phaseB:
    #    label.append('(%d,%d)' % (x,y))
    #plt.xlabel(label)
    plt.show()

if __name__ == "__main__":
    fn = sys.argv[1]
    #print (fn)
    phaseA, phaseB, Rssi = load_dataset(fn)
    plot_rx_pattern(phaseA, phaseB, Rssi)
    plt.show()
