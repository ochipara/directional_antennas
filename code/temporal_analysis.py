import pandas as pd
import numpy as np
import itertools
import sys
import matplotlib.pyplot as plt
import os
import pickle
import math
from sklearn import cluster


def compute_differences(data, num_phases, threshold, debug = False):
    """
    identifies pairs of rows (i, j) such that send_time[j] - sent_time[i] > dt
    each such pair is added to a dataset:
        (i, j, dt, drssi)

    :param df:
    :return:
    """
    df = data.df
    N = len(df)
    i = 0
    data = []
    dtime = []
    while i < N:
        j = i + 1

        rowi_send_time = df.send_time[i]
        rowi_rssi = df.rssi[i]
        rowi_phase = df.phase[i]

        while j < N:
            rowj_send_time = df.send_time[j]
            rowj_rssi = df.rssi[j]
            rowj_phase = df.phase[j]
            dt = rowj_send_time - rowi_send_time
            dphase = abs(rowi_phase - rowj_phase) #min(abs(rowi_phase - rowj_phase), abs(abs(rowi_phase - rowj_phase) - 255))

            if dt > 0:
                if debug: print(i, j, rowi_phase, rowj_phase, dt)
                if dt > threshold:
                    break

                if rowi_phase < rowj_phase:
                    data.append((i, j, rowi_phase, rowj_phase, dt, dphase, abs(rowi_rssi - rowj_rssi)))
                else:
                    data.append((j, i, rowj_phase, rowi_phase, dt, dphase, abs(rowi_rssi - rowj_rssi)))
                dtime.append(dtime)
            else:
                if debug: print('Skipping', i, j)
            j += 1
        i += 1

    delta_df = pd.DataFrame(data)
    delta_df.columns = ('i', 'j', 'phase_i', 'phase_j', 'dt', 'dphase', 'drssi')
    return delta_df


def compute_round_prr(data):
    df = data.df
    rounds = np.unique(df.exp_round)
    packets_per_round = []
    for r in rounds:
        pkts = len(df.exp_round[df.exp_round == r])
        packets_per_round.append(pkts)
    return packets_per_round


def simple_hist(x):
    vals = np.unique(x)
    h = {}
    for v in vals:
        h[v] = 0
    for val in x:
        h[val] += 1
    return h


def compute_best_phase_seq(df):
    exp_rounds = np.unique(df.exp_round)
    best_config = []
    curr_config = 0
    for r in exp_rounds:
        df_round = df[df.exp_round == r]
        best_rssi = max(df_round.rssi)
        min_time = min(df_round.send_time)
        config = df_round.phase[df_round.rssi == best_rssi].values

        if curr_config not in config:
            curr_config = np.random.choice(config)
        best_config.append((min_time, curr_config))

    df = pd.DataFrame(best_config)
    df.columns = ['time', 'phase']
    return df


def phase_selection0(data):
    phase_seq = []
    selected = None
    unique_phases = data.unique_phases()
    for r in range(data.num_rounds()):
        if selected is not None and np.isnan(data.get_rssi(r, selected)) == False:
            pass
        else:
            round_rssis = np.array([data.get_rssi(r, p) for p in unique_phases])
            selected = unique_phases[np.nanargmax(round_rssis)]
        assert(selected is not None and math.isnan(selected) == False)
        phase_seq.append(selected)

    return phase_seq


class ExperimentalData:
    def __init__(self, fn):
        self.fn = fn
        self.dir = os.path.dirname(fn)
        self.filename = fn.split('/')[-1]
        self.base_filename = '.'.join(self.filename.split('.')[0:-1])
        self.best_config_seq = None
        self.rp_index = None
        self.df = pd.DataFrame.from_csv(self.fn)
        self.load_index()

    def load_index(self):
        fn = '%s/%s_rpindex.pickle' % (self.dir, self.base_filename)
        if os.path.isfile(fn):
            with open(fn, 'rb') as f:
                self.rp_index = pickle.load(f)
                return

        print('building index ... will take a bit of time')
        self.rp_index = {}
        for index, row in self.df.iterrows():
            k = '%d-%d' % (row['exp_round'], row['phase'])
            if k in self.rp_index:
                raise ValueError('Key already in index %s' % k)
            self.rp_index[k] = (row.send_time, row.rssi, row.lqi)

        with open(fn, 'wb') as f:
            pickle.dump(self.rp_index, f, pickle.HIGHEST_PROTOCOL)

    def compute_best_phase_seq(self):
        if self.best_config_seq is not None:
            return self.best_config_seq

        fn = '%s/%s_best_config.pickle' % (self.dir, self.base_filename)
        if os.path.isfile(fn):
            with open(fn, 'rb') as f:
                self.best_config_seq = pickle.load(f)
            return self.best_config_seq

        self.best_config_seq = compute_best_phase_seq(self.df)
        with open(fn, 'wb') as f:
            pickle.dump(self.best_config_seq, f, pickle.HIGHEST_PROTOCOL)

        return self.best_config_seq

    def compute_differences(self, threshold):
        fn = '%s/%s_delta_%d.csv' % (self.dir, self.base_filename, threshold)
        if os.path.isfile(fn):
            df = pd.DataFrame.from_csv(fn)
            return df

        print('computing differences')
        df = compute_differences(self, len(self.unique_phases()), threshold)
        df.to_csv(fn)
        print('done')
        return df


    def get_rssi(self, exp_round, phase):
        k = '%d-%d' % (exp_round, phase)
        if k in self.rp_index:
            t = self.rp_index[k]
            return t[1]
        else:
            return np.nan

    def unique_phases(self):
        phases = np.unique(self.df.phase)
        return phases

    def num_rounds(self):
        return max(self.df.exp_round) + 1


def eval_phase_seq(data, phase_seq):
    rssi = []
    for r in range(len(phase_seq)):
        rssi_val = data.get_rssi(r, phase_seq[r])
        rssi.append(rssi_val)
    return rssi


def plot_rssi_phase(data):
    plt.figure()

    df = data.df
    phases = np.unique(df.phase)

    rssi = {}
    for phase in phases:
        rssi[phase] = np.mean(df.rssi[df.phase == phase])

    x = sorted(rssi.keys())
    y = [rssi for phase, rssi in sorted(zip(rssi.keys(), rssi.values()))]
    plt.plot(x, y, '-o')
    plt.xlabel('Phase')
    plt.ylabel('RSSI (dBm)')
    plt.tight_layout() #gcf().subplots_adjust(right=.4)


def plot_rssi_time(data, phases):
    df = data.df
    min_time = min(df.send_time)
    for phase in phases:
        tmp = df[df.phase == phase]
        x = tmp.send_time.values
        x = x - min_time
        x = x / 1000.0
        plt.plot(x, tmp.rssi, '.-')
    plt.xlim([0, max(x)])
    plt.xlabel("Time (s)")
    plt.ylabel('RSSI (dBm)')
    plt.legend(['$\phi$=%d' % d for d in phases], loc='lower right', ncol=2)


def plot_prr_time(data):
    R = data.num_rounds()

    prr = []
    phases = data.unique_phases()
    for phase in phases:
        count = 0
        for r in range(R):
            if ~np.isnan(data.get_rssi(r, phase)):
                count += 1
        prr.append(float(count) / R * 100.0)

    ind = np.arange(0,len(phases))
    plt.bar(ind, prr)
    plt.xticks(ind + .4, ['%d' % x for x in phases], rotation='vertical')
    plt.xlim([0, len(phases)])
    plt.xlabel('Phase')
    plt.ylabel('PRR (%)')


def plot_best_seq(data):
    best_phase_seq = data.compute_best_phase_seq()
    print(best_phase_seq.describe())
    h = simple_hist(best_phase_seq.phase.values)
    max_freq = max(h.values())
    for (best_single_phase, freq) in h.items():
        if freq == max_freq:
            break

    print('best single phase', best_single_phase)
    x = (best_phase_seq.time - min(best_phase_seq.time)) / 1000.0
    f1 = plt.figure()
    plt.plot(x, best_phase_seq.phase)
    plt.axhline(y=best_single_phase, color='r', linewidth=4)
    plt.xlim(0,max(x))
    plt.xlabel('Time (s)')
    plt.ylabel('Phase with best RSSI')


    f2 = plt.figure()
    for phase in data.unique_phases():
        if phase not in h:
            h[phase] = 0

    pdata = sorted(zip(h.keys(), h.values()))
    y = np.array([t[1] for t in pdata])

    inds = np.arange(0, len(data.unique_phases())) + .5
    norm = sum(y)
    y = y / norm
    plt.bar(inds, y, 1)
    plt.xticks(inds + .5, ['%d' % l for l in data.unique_phases()], rotation='vertical')
    plt.xlabel('Phase')
    plt.ylabel('Probability having best RSSI')

    best_rssi = eval_phase_seq(data, best_phase_seq.phase.values)
    single_rssi = eval_phase_seq(data, [best_single_phase] * len(best_phase_seq))
    alg0_seq = phase_selection0(data)
    alg0_rssi = eval_phase_seq(data, alg0_seq)
    print('best seq', np.nanpercentile(best_rssi, [0, 25, 50, 75, 100]))
    print('single_rssi seq', np.nanpercentile(single_rssi, [0, 25, 50, 75, 100]))
    print('alg0_rssi seq', np.nanpercentile(alg0_rssi, [0, 25, 50, 75, 100]))


    # http://www.andata.at/en/software-blog-reader/why-we-love-the-cdf-and-do-not-like-histograms-that-much.html
    f3 = plt.figure()
    x = np.arange(0, 101)
    y1 = np.percentile(best_rssi, x)
    single_rssi = np.array(single_rssi)
    single_rssi = single_rssi[~np.isnan(single_rssi)]
    y2 = np.percentile(single_rssi, x)
    y3 = np.percentile(alg0_rssi, x)
    x = x / 100.0
    plt.plot(y1, x)
    plt.plot(y2, x)
    plt.plot(y3, x)
    plt.xlabel('RSSI (dBm)')
    plt.ylabel('CDF')
    plt.legend(['best', '$\phi=%d$' % best_single_phase, 'single-rssi'], loc='lower right')

    return f1, f2, f3


def plot_phase_time_variability(data, time_threshold, time_step):
    differences = data.compute_differences(time_threshold)
    time_cluster = np.array(differences.dt)
    time_cluster = np.floor(time_cluster / time_step)
    dphase = np.array(differences.dphase)
    num_time_clusters = int(max(time_cluster) + 1)
    phases = data.unique_phases()

    # td_x = []
    # td_y = []
    # td_z = []
    #d = {}
    C = np.zeros((num_time_clusters, len(phases)))
    C = C * np.nan

    f1 = plt.figure()
    for cluster in np.arange(num_time_clusters):
        y = []
        for phase_ix in range(len(phases)):
            phase = phases[phase_ix]
            ix = (time_cluster == cluster) & (dphase == phase)
            mrssi = differences.drssi[ix]
            # td_x.append(cluster)
            # td_y.append(phase)

            if len(mrssi) > 0:
                val = np.mean(mrssi)
            else:
                val = np.nan

            y.append(val)
            C[cluster, phase_ix] = val
            #td_z.append(val)

        plt.plot(phases, y)
    plt.xlabel('Phase difference')
    plt.ylabel('RSSI difference (dBm)')
    #plt.legend([str(phases) for phases in np.arange(num_time_clusters)], loc='lower right', ncol=8)


    from mpl_toolkits.mplot3d import Axes3D
    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # ax.scatter(td_x, td_y, td_z)


    # print(C.shape)
    # for x in range(len(phases)):
    #     for y in range(num_time_clusters):
    #         phase = phases[x]
    #         val = d['%d-%d' % (y, phase)]
    #         C[x, y] = val
    #         print('%.3f ' % val, end='')
    #     print()

    f2 = plt.figure()
    Cm = np.ma.masked_invalid(C)
    plt.pcolor(Cm.T)
    plt.colorbar()
    plt.ylabel('Phase difference')
    plt.xlabel('Time difference')
    ind = np.arange(0, num_time_clusters, 4)
    plt.xticks(ind, ind * time_step)
    ind = np.arange(0, len(phases), 4)
    plt.yticks(ind, phases[ind])

    f3 = plt.figure()
    plt.subplot(2,1,1)
    plt.plot(phases, np.nanstd(C, axis=0))
    plt.ylim([0, 2.5])
    plt.xlabel('Phase difference')
    plt.ylabel('STDEV of RSSI')
    plt.legend(['Phase diff'], loc='upper right')

    plt.subplot(2,1,2)
    plt.plot(np.arange(num_time_clusters) * time_step, np.nanstd(C, axis=1), 'r-')
    plt.xlabel('Time difference')
    plt.ylabel('STDEV of RSSI')
    plt.ylim([0, 2.5])
    plt.legend(['Time diff'], loc='lower right')

    return f1, f2, f3

if __name__ == "__main__":
    fn = '/Users/ochipara/Working/DirectionalAntennas/data/testbed/tx_temporal/AB_Power7_Channel11/2016-09-25_NODE0.csv'
    node0_ch11_pw11 = ExperimentalData(fn)
    sys.exit(0)

    #fn = '/Users/ochipara/Working/DirectionalAntennas/data/testbed/rp-power31/2016-09-20_NODE10.csv'
    fn='/Users/ochipara/Working/DirectionalAntennas/data/testbed/rp_phase32_ch11_pw11/2016-09-21_NODE0.csv'


    #data = ExperimentalData(fn)

    #plot_rssi_phase(data)

    # plt.figure()
    # plot_rssi_time(node0_ch11_pw11, np.arange(0, 256, 64))
    # plt.savefig('rssi_time_n0_ch11.pdf')
    # plt.show()

    # plot the prr over time
    # plt.figure()
    # plot_prr_time(node0_ch11_pw11)
    # plt.show()

    # plot diagnostics for selecting the data
    # plot_best_seq(data)


    # plot the differences in time and phase
    plot_phase_time_variability(node0_ch11_pw11, 1000, 8)
    # plot_phase_impact(data, differences)



    # plt.imshow(C)


    # y, phases = np.mgrid[0:num_time_clusters, phases]
    # z = np.zeros(y.shape)
    # for xval in phases:
    #     for yval in y:
    #         z[xval, yval] = d['%d-%d' % (yval, xval)]
    #
    #
    # print(y, phases)


    plt.show()


#        print('cluster', c, 'members', ' '.join([str(x) for x in cluster_map[c]]))








   #@ plt.show()
    sys.exit(0)


    phases = sorted(h.keys())



    # plt.figure()
    # plt.hist(best_config, bins=255)
    plt.show()
    # print(best_config)

    sys.exit(0)



    # pkts_per_round = compute_round_prr(df)
    # print(pkts_per_round)
    # sys.exit(0)

    # delta_df = compute_differences(df, threshold=6000, debug=True)
    # delta_df.to_csv('delta.csv')
    # sys.exit(0)

    delta_df = pd.DataFrame.from_csv('delta.csv')

    phases = sorted(delta_df.dt.values)
    plt.figure(1)
    plt.plot(phases)

    """
    Plot:
    y axis --- rssi
    x axis --- phase difference
    """

    phase_diffs = np.unique(delta_df.dphase)
    # print(phase_diffs)
    #
    # time_bins = [(0,10), (10, 20), (20, 30), (30, 40), (40, 50), (50, 60)]
    # #time_bins = [(0, 2500), (3000, 5000)]
    # for (time_lo, time_hi) in time_bins:
    #     print(time_lo, time_hi)
    #     y = []
    #     bdata = []
    #     for pdiff in phase_diffs:
    #         d = delta_df[(delta_df.dphase == pdiff) & (delta_df.dt > time_lo) & (delta_df.dt <= time_hi)]
    #         #print(d.describe())
    #         mrssi = np.mean(d.drssi)
    #         # d = d[['dt', 'dphase', 'drssi']]
    #         y.append(mrssi)
    #         bdata.append(d.drssi.values)
    #         print(pdiff, mrssi)
    #         #break
    #
    #     plt.figure(2)
    #     plt.plot(phase_diffs, y)
    #     plt.figure()
    #     plt.boxplot(bdata)
    #     plt.title('time=%d,%s' % (time_lo, str(time_hi)))
    #
    #
    # plt.figure(2)
    # plt.title('Variation over time and phase')
    # plt.xlabel('Diff Phase')
    # plt.ylabel('Diff RSS')
    # plt.legend(['[%d,%s]' % (l,str(h)) for (l, h) in time_bins], loc='lower right')
    # plt.savefig('channel-time.pdf')

    plt.figure()

    from sklearn.cluster import AgglomerativeClustering, KMeans, MiniBatchKMeans
    ag = KMeans(n_clusters=8)
    to_cluster = np.array(phase_diffs).reshape(-1, 1)
    ag.fit(to_cluster)

    phase_clusters = {}
    for best_single_phase in phase_diffs:
        cluster = ag.predict(float(best_single_phase))[0]
        if cluster not in phase_clusters:
            phase_clusters[cluster] = [best_single_phase]
        else:
            phase_clusters[cluster].append(best_single_phase)
#    [(phase[0], ag.predict(float(phase))[0]) for phase in phase_diffs]
#
#     print(phase_clusters)
#     for phase in phase_clusters:
#         subset = delta_df[delta_df.dphase.isin(phase_clusters[phase])]
#         x = []
#         y = []
#         time_bins = [(0, 10), (10, 20), (20, 30), (30, 40), (40, 50), (50, 60), (60, 70)]
#         for (lo, hi) in time_bins:
#             x.append(np.mean([lo, hi]))
#             val = np.mean(subset.drssi[subset.dt.isin([lo, hi])])
#             y.append(val)
#         plt.plot(x,y)

    print(phase_clusters)
    for best_single_phase in phase_clusters:
        subset = delta_df[delta_df.dphase.isin(phase_clusters[best_single_phase])]
        print(subset)
        phases = [] #np.unique(subset.dt)
        y = []
        print(len(phases))
        # plt.hist(x)
        # plt.show()
        # sys.exit(0)
        time_bins = [(0, 10), (10, 20), (20, 30), (30, 40), (40, 50), (50, 60), (60, 70)]
        for (lo, hi) in time_bins:
            phases.append(np.mean([lo, hi]))
            val = np.mean(subset.drssi[subset.dt.isin(list(range(lo, hi + 1)))])
            y.append(val)
        plt.plot(phases, y)
    plt.show()


