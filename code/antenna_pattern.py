from experiments import load_experiment
import numpy as np
import matplotlib.pyplot as plt
import argparse

def plot_one_pattern(ax, df, phase, missing='min', debug=False):
    """
    Plots the antenna pattern
    Input: an experiment and a single phase
    """
    df2 = df[df.phase == phase]

    unique_angles = np.unique(df.angle)

    if debug: print(df2)

    #
    # compute a dictionary from angles to the average rssi
    # you probably want a single phase orientation
    #
    mean_rssi = {}
    for angle in unique_angles:
        val = df2.rssi[df2.angle == angle]
        if len(val) == 1:
            mean_rssi[angle] = val.values[0]
        elif len(val) == 0:
            if missing == 'min':
                mean_rssi[angle] = min(df.rssi)
            elif missing == 'nan':
                mean_rssi[angle] = np.nan
            else:
                mean_rssi[angle] = missing
        else:
            print('Warning: multiple rssi values found')
            mean_rssi[angle] = np.mean(val)

    #
    # the angles may not be complete so, we can expand the pattern all the way around
    #
    if 0 in mean_rssi:
        mean_rssi[360] = mean_rssi[0]
    else:
        print('Warning: 0 not found')
    keys = sorted(list(mean_rssi.keys()))

    x = np.array(keys) * 2 * np.pi / 360
    y = [mean_rssi[k] for k in keys]
    assert (len(x) == len(y))

    if debug:
        print('\n\nDATA %s' % ('=' * 30))
        for ix in range(len(x)):
            print(x[ix], y[ix])

        print('\n\nRANGE %s' % ('=' * 30))

    line, = ax.plot(x, y, linewidth=3)
    return (line, min(y), max(y))


def plot_antenna_pattern(df, phases, power, axis=None, missing='min', debug=False):
    """
    Plots the antenna pattern
    Input: an experiment and a set of phases that need to be plotted
    """

    df = df[df.power_level == power]
    if len(df) == 0:
        print('Warning: no data at power level %d' % power)
        return None

    if axis is None:
        fig = plt.figure(figsize=(8, 8))
        ax = fig.add_axes([0.1, 0.1, 0.8, 0.8], projection='polar')
    else:
        ax = axis

    global_max_y = None
    global_min_y = None
    for ix in range(len(phases)):
        phase = phases[ix]
        (line, min_y, max_y) = plot_one_pattern(ax, df, phase, missing=missing, debug=debug)
        if global_max_y is None: global_max_y = max_y
        if global_min_y is None: global_min_y = min_y
        global_max_y = max(global_max_y, max_y)
        global_min_y = min(global_min_y, min_y)
        line.set_label('%d' % phase)

    step = np.pi * 2 / 50
    r = np.arange(0, np.pi * 2 + step, step)
    ax.plot(r, [global_max_y] * len(r), color='b', ls='--', linewidth=1)  # , label='max=%.0f dBm' % maxy)
    ax.plot(r, [global_min_y] * len(r), color='r', ls='--', linewidth=1)  # , label='min=%.0f dBm' % miny)
    # ax.grid(True)
    ax.set_rmax(global_max_y * 0.9)
    ax.set_rmin(global_min_y * 1.1)
    # ax.text(22.5 * 2 * np.pi / 360, global_max_y * 1.05, 'max=%.0f dBm' % global_max_y)
    # ax.text(22.5 * 2 * np.pi / 360, global_min_y * 1.05, 'min=%.0f dBm' % global_min_y)

    font = {'weight': 'bold', 'color': 'g'}
    ax.text(0 * 2 * np.pi / 360, global_max_y * 1, '%.0f\ndBm' % global_max_y, fontdict=font)
    ax.text(15 * 2 * np.pi / 360, global_min_y * .99, '%.0f\ndBm' % global_min_y, fontdict=font)

    # moving the location of the legend
    # http://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
    ax.legend(bbox_to_anchor=(1.1, 1.05))
    return ax

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('file', nargs=1, type=str, help='experiment to process')
    parser.add_argument('--power', nargs='?', type=int, default=31, help='power level to be used for processing')
    parser.add_argument('--missing', nargs='?', type=str, default='min',
                        help='how to treat missing values. you may use min/nan/or an int')
    parser.add_argument('--show', action='store_const', const=True, help='show the figure')

    args = parser.parse_args()
    arg_dict = vars(args)
    fn = arg_dict['file'][0]
    power = arg_dict['power']
    missing = arg_dict['missing']
    show = arg_dict['show']

    print('fn=%s power=%d missing=%s show=%s' % (fn, power, missing, show))

    e = load_experiment(fn)
    df = e.get_dataframe()

    phases = list(np.unique(df.phase))
    ax = plot_antenna_pattern(df, phases, power, missing=missing, debug=False)
    base_fn = fn[:fn.rfind('.')]
    fn_out = base_fn + '_antenna.pdf'
    if ax is not None:
        plt.savefig(fn_out)
    if show:
        plt.show()
