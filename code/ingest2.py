"""
This is a simple script to ingest the data from a log file.
It will go through and import all the relevant data.
The script will through a lot of errors if the data is not in the format that it expects.
Most of the issues are related to when a data line is split in two. For example,

Test:	4964	31	95	100
98	-38	106	0	53

will cause an error. You can fix it manually by putting:

Test:	4964	31	95	100	98	-38	106	0	53

After Dhruv fixes the serial code, we can tighten the requirements.
"""

import sys
import re
from experiments import Experiment2

rx_start_re = re.compile(r"Rx:\s+START")
rx_done_re = re.compile(r"Rx:\s+DONE")


def process_file(fn):
    print('ingesting %s' % fn)
    with open(fn, 'r') as f:
        lines = f.readlines()

    experiments = []
    experiment = None
    header = None
    line_count = 0
    for line in lines:
        line = line.strip()
        line_count += 1

        if len(line) > 0:
            if line.startswith('Platform'):
                print('=== Header ===')
                (tag, value) = line.split(':')
                experiment = Experiment2(value.strip())
                experiments.append(experiment)
                header = True
            elif header is True:
                if line.startswith('Start_delay') or line.startswith('Angle_'):
                    vals = re.split('\s+', line)
                    for val in vals:
                        val = val.strip()
                        if len(val) > 0:
                            # print(val)
                            (tag, value) = val.split('=')
                            if tag == 'Start_delay':
                                pass
                            elif tag == 'Send_delay':
                                experiment.send_delay = int(value)
                            elif tag == 'Send_count':
                                experiment.send_count = int(value)
                            elif tag == 'Angle_step':
                                experiment.angle_step = int(value)
                            elif tag == 'Angle_count':
                                experiment.angle_count = int(value)
                            else:
                                raise Exception('Cannot interpret metadata tag %s' % vals)
                elif line.startswith('TX_power'):
                    (tag, val) = line.split(':')
                    tag = tag.strip()
                    val = val.strip()
                    if tag == 'TX_power':
                        try:
                            pwrs = []
                            for x in val.split():
                                pwrs.append(int(x))
                            experiment.tx_power = pwrs
                        except ValueError:
                            raise ValueError('val=%s line=%d' % (val, line_count))
                    else:
                        raise Exception('Cannot interpret metadata [%s] [lineno=%d]' % (line, line_count))
                elif line.startswith('Ant_config:'):
                    (tag, vals) = line.split(':')
                    val2 = re.split('\s+', vals)
                    args = []
                    for v in val2:
                        v = v.strip()
                        if len(v) > 0: args.append(v)
                    experiment.antenna_config = args
                elif rx_start_re.match(line) is not None:
                    print(experiment.print_header())
                    print('==== Data ====')
                    header = False
                else:
                    raise Exception('Cannot interpret %s' % line)
            else:
                if rx_start_re.match(line) is not None: continue
                if rx_done_re.match(line) is not None: continue
                if line.startswith('Done received'): continue
                if line.startswith('Packet count'): continue

                tuples = re.split('\s+', line)
                if len(tuples) == 7:
                    (recv_time, sender_seq, rssi, lqi, power_level, angle, phase) = tuples
                    try:
                        experiment.add_data(recv_time, sender_seq, rssi, lqi, power_level, angle, phase)
                    except Exception:
                        raise Exception(line)
                else:
                    raise Exception(
                        'Cannot interpret data line [%s] [ln=%d] [tuples=%d]' % (line, line_count, len(tuples)))

    return experiments


if __name__ == "__main__":
    fn = sys.argv[1]
    experiments = process_file(fn)
    print('Loaded %d experiments' % len(experiments))

    base_fn = fn[:fn.rfind('.')]
    count = 0
    for experiment in experiments:
        fn_out = base_fn + "_" + str(count) + ".csv"
        print('Saving experiment to %s' % fn_out)
        experiment.save_csv(fn_out)
        count += 1
