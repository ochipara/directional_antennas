"""
This is a simple script to ingest the data from a log file.
It will go through and import all the relevant data.
The script will through a lot of errors if the data is not in the format that it expects.
Most of the issues are related to when a data line is split in two. For example,

Test:	4964	31	95	100	
98	-38	106	0	53

will cause an error. You can fix it manually by putting:

Test:	4964	31	95	100	98	-38	106	0	53

After Dhruv fixes the serial code, we can tighten the requirements.
"""

import sys
import re
import pandas as pd
import numpy as np

data_line_re = re.compile(r"Test:|T?st:|Te?st:")
rx_start_re = re.compile(r"Rx:\sSTART")


class Experiment:
    """
    This class holds the data for an experiment.
    """

    def __init__(self, platform):
        self.platform = platform
        self.send_delay = None
        self.send_count = None
        self.angle_step = None
        self.angle_count = None
        self.data = []
        self.data_header = ['seq', 'power', 'angle', 'phase', 'prr', 'rssi_avg', 'lqi_avg', 'rssi_std', 'lqi_std']
        self.power_level = []
        self.angle = []
        self.phase = []
        self.prr = []
        self.rssi_avg = []
        self.lqi_avg = []

    def add_data(self, seq, power_level, phy_angle, antenna_phase, prr, rssi_avg, lqi_avg, rssi_std, lqi_std):
        self.power_level.append(int(power_level))
        self.angle.append(int(np.around(float(phy_angle) / 200.0 * 360.0)))
        self.phase.append(int(antenna_phase))
        self.prr.append(int(prr))
        self.rssi_avg.append(float(rssi_avg))
        self.lqi_avg.append(float(lqi_avg))

        self.data.append((seq, power_level, phy_angle, antenna_phase, prr, rssi_avg, lqi_avg, rssi_std, lqi_std))

    def get_dataframe(self):
        return pd.DataFrame({'phase': self.phase,
                             'angle': self.angle,
                             'power_level': self.power_level,
                             'rssi': self.rssi_avg})

    def unique_phases(self):
        return np.unique(self.phase)

    def filter_data(self, attr, filter_power_level=None):
        """
        TODO: add more things to filter on
        """
        tofilter = getattr(self, attr)
        r = []

        for index in len(tofilter):
            include = True
            if filter_power_level is not None:
                power = self.power_level[index]
                include = include and (power in filter_power_level)

            if include:
                r.append(tofilter[index])

        return r

    def print_header(self):
        return 'platform=%s send_delay=%d send_count=%d' % (self.platform, self.send_delay, self.send_count)

    def save_csv(self, name):
        with open(name, 'w') as f:
            # write the metadata for the file
            f.write('send_delay,%d\n' % self.send_delay)
            f.write('send_count,%d\n' % self.send_count)
            f.write('angle_step,%d\n' % self.angle_step)
            f.write('angle_count,%d\n' % self.angle_count)
            f.write('\n\n')

            # write the data
            f.write(','.join(self.data_header) + '\n')
            for item in self.data:
                line = ','.join(item)
                f.write(line + '\n')

    def __str__(self):
        return self.print_header() + ' data=%d' % len(self.data)


def load_experiment(fn):
    experiment = Experiment('phaser')
    with open(fn, 'r') as f:
        lines = f.readlines()

    metadata = True
    for line in lines:
        line = line.strip()
        if len(line) > 0:
            if metadata:
                (tag, value) = line.split(',')
                setattr(experiment, tag, int(value))
            else:
                if line.startswith('seq'):
                    pass
                else:
                    (
                    seq, power_level, phy_angle, antenna_phase, prr, rssi_avg, lqi_avg, rssi_std, lqi_std) = line.split(
                        ',')
                    experiment.add_data(seq, power_level, phy_angle, antenna_phase, prr, rssi_avg, lqi_avg, rssi_std,
                                        lqi_std)
        else:
            metadata = False

    return experiment


def process_file(fn):
    print('ingesting %s' % fn)
    with open(fn, 'r') as f:
        lines = f.readlines()

    experiments = []
    experiment = None
    header = None
    line_count = 0
    for line in lines:
        line = line.strip()
        line_count += 1

        if len(line) > 0:
            if line.startswith('Platform'):
                print('=== Header ===')
                (tag, value) = line.split(':')
                experiment = Experiment(value.strip())
                experiments.append(experiment)
                header = True
            elif header is True:
                if line.startswith('Start_delay') or line.startswith('Angle_'):
                    vals = re.split('\s+', line)
                    for val in vals:
                        val = val.strip()
                        if len(val) > 0:
                            # print(val)
                            (tag, value) = val.split('=')
                            if tag == 'Start_delay':
                                pass
                            elif tag == 'Send_delay':
                                experiment.send_delay = int(value)
                            elif tag == 'Send_count':
                                experiment.send_count = int(value)
                            elif tag == 'Angle_step':
                                experiment.angle_step = int(value)
                            elif tag == 'Angle_count':
                                experiment.angle_count = int(value)
                            else:
                                raise Exception('Cannot interpret metadata tag %s' % vals)
                elif line.startswith('TX_power'):
                    (tag, val) = line.split(':')
                    tag = tag.strip()
                    val = val.strip()
                    if tag == 'TX_power':
                        try:
                            pwrs = []
                            for x in val.split():
                                pwrs.append(int(x))
                            experiment.tx_power = pwrs
                        except ValueError:
                            raise ValueError('val=%s line=%d' % (val, line_count))
                    else:
                        raise Exception('Cannot interpret metadata [%s] [lineno=%d]' % (line, line_count))
                elif line.startswith('Ant_config:'):
                    (tag, vals) = line.split(':')
                    val2 = re.split('\s+', vals)
                    args = []
                    for v in val2:
                        v = v.strip()
                        if len(v) > 0: args.append(v)
                    experiment.antenna_config = args
                elif rx_start_re.match(line) is not None:
                    print(experiment.print_header())
                    print('==== Data ====')
                    header = False
                else:
                    raise Exception('Cannot interpret %s' % line)
            elif data_line_re.match(line) is not None:
                tuples = re.split('\s+', line)
                if len(tuples) == 10:
                    (
                    tag, seq, power_level, phy_angle, antenna_phase, prr, rssi_avg, lqi_avg, rssi_std, lqi_std) = tuples
                    try:
                        experiment.add_data(seq, power_level, phy_angle, antenna_phase, prr, rssi_avg, lqi_avg,
                                            rssi_std, lqi_std)
                    except Exception:
                        raise Exception(line)
                else:
                    raise Exception(
                        'Cannot interpret data line [%s] [ln=%d] [tuples=%d]' % (line, line_count, len(tuples)))
            else:
                # raise Exception('Cannot match line [%s] [lineno=%d]' % (line, line_count))
                pass
    return experiments


if __name__ == "__main__":
    fn = sys.argv[1]
    experiments = process_file(fn)
    print('Loaded %d experiments' % len(experiments))

    base_fn = fn[:fn.rfind('.')]
    count = 0
    for experiment in experiments:
        fn_out = base_fn + "_" + str(count) + ".csv"
        print('Saving experiment to %s' % fn_out)
        experiment.save_csv(fn_out)
        count += 1
