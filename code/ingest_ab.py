"""
This is a simple script to ingest the data from a log file.
It will go through and import all the relevant data.
The script will through a lot of errors if the data is not in the format that it expects.
Most of the issues are related to when a data line is split in two. For example,

Test:	4964	31	95	100
98	-38	106	0	53

will cause an error. You can fix it manually by putting:

Test:	4964	31	95	100	98	-38	106	0	53

After Dhruv fixes the serial code, we can tighten the requirements.
"""

import sys
import re
import pandas as pd

data_line_re = re.compile(r"Test:|T?st:|Te?st:")
rx_start_re = re.compile(r"Rx:\sSTART")


def check_data(tuples):
    (seq, power, angle, phase, prr, rssi_avg, lqi_avg, rssi_std, lqi_std, phase1, phase2, line_count) = tuples
    if phase1 < 0 or phase2 < 0 or phase < 0:
        raise ValueError('Phase cannot be smaller than zero [ln=%d]' % line_count)

    if rssi_avg > 0:
        raise ValueError('RSSI cannot be greater than zero [ln=%d]' % line_count)

    if lqi_avg < 0:
        raise ValueError('LQI cannot be smaller than zero [ln=%d]' % line_count)

    if angle < 0 or angle > 200:
        raise ValueError('Angle outslide stepper range [ln=%d]' % line_count)

def is_data(line, prev_line):
    if len(line) == 0: return False
    count = 0
    for x in line:
        if x in ['0','1','2','3','4','5','6','7','8','9','-','\t','\n', ' ']:
            count = count + 1
        else:
            pass
    r = count / len(line)
    if r > .9:
        if prev_line is not None:
            if prev_line.find('TX_power:') >= 0: return False
            if prev_line.find('Ant_config:') >= 0: return False
            if prev_line.find('Angle_') >= 0: return False
            if prev_line.find('Send_') >= 0: return False

        return True
    else:
        return False


def fix_test(line):
    tag = 'Test:'
    if len(line) < len(tag): return line

    count = 0
    for x in range(len(tag)):
        if line[x] == tag[x]:
            count += 1
    if count >= 2:
        line = tag + line[len(tag):]

    return line

def process_file(fn):
    # print('ingesting %s' % fn)
    with open(fn, 'r') as f:
        lines = f.readlines()

    header = None
    header_count = 0
    line_count = 0
    data = []
    while line_count < len(lines):
        line = lines[line_count]
        line = fix_test(line)
        line_count += 1

        if len(line) > 0:
            if line.startswith('Platform'):
                header = True
                assert (header_count == 0)
                header_count += 1
            elif rx_start_re.match(line) is not None:
                header = False
            elif line.find('Rx: DONE') >= 0 or line.find('Start_delay') >=0 or line.find('TX_power:') >= 0:
                continue
            elif header is False:
                # try:
                if line_count < len(lines):
                    next_line = lines[line_count]
                    if is_data(next_line, None):
                        if line[-1] == '\n':
                            line = line[:-1]
                        line = line + next_line
                        line = line.strip()
                        line_count += 1

                try:
                    process_line(data, line, line_count)
                except:
                    if line[-1] == '\n':
                        line = line[:-1]
                    line = line + next_line
                    line = line.strip()
                    line_count += 1
                    process_line(data, line, line_count)

            elif is_data(line, None) and len(line.strip()) > 0:
                raise Exception('Cannot match line [%s] [lineno=%d]' % (line, line_count))
            else:
                print('Skipping', line_count, line.strip())

    df = pd.DataFrame(data)
    df.columns = ['seq', 'power', 'angle', 'phase', 'prr', 'rssi_avg', 'lqi_avg', 'rssi_std', 'lqi_std', 'phase1',
                  'phase2', 'line_count']
    return df


def process_line(data, line, line_count):
    line = line.strip()
    tuples = re.split('\s+', line)
    if len(tuples) == 12:
        try:
            tuples = [int(t) for t in tuples[1:]]
            (seq, power, angle, phase, prr, rssi_avg, lqi_avg, rssi_std, lqi_std, phase1, phase2) = tuples
            rssi_avg -= 45
            tuples = (seq, power, angle, phase, prr, rssi_avg, lqi_avg, rssi_std, lqi_std, phase1, phase2, line_count)

            check_data(tuples)

            data.append(tuples)
        except Exception:
            raise Exception(line)
    else:
        raise Exception(
            'Cannot interpret data line [%s] [ln=%d] [tuples=%d]' % (line, line_count, len(tuples)))


if __name__ == "__main__":
    fn = sys.argv[1]
    df = process_file(fn)
    print(df.describe())

    base_fn = fn[:fn.rfind('.')]
    fn_out = base_fn + ".csv"
    print('Saving to', fn_out)
    df.to_csv(fn_out)


