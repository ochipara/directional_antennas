import sys
import re
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

def plot_pattern(df, power_level, phase_filter, figure=None):
    df = df[df.power == power_level]
    if figure is None:
        fig = plt.figure()
    else:
        fig = figure
    ax = fig.add_axes([0.02, 0.02, 0.98, 0.98], projection='polar')

    for phase in phase_filter:
        grp = df[df.phase == phase].groupby(['angle']).mean()
        a = np.array(grp.index.values)
        a = a / 200 * 2 * np.pi
        a = np.append(a, 2 * np.pi)
        y = grp.rssi.values
        y = np.append(y, y[0])

        ax.plot(a, y)

    plt.ylim(min(df.rssi) * 1.05, max(df.rssi) *.95)
    plt.yticks(np.arange(np.floor(min(df.rssi) * 1.05 / 10) * 10, np.ceil(max(df.rssi) * .95 / 10) * 10, 10))

    return fig

def plot_pattern2(df, power_level, phase_filter, figure=None):
    df = df[df.power == power_level]
    if figure is None:
        fig = plt.figure()
    else:
        fig = figure
    ax = fig.add_axes([0.02, 0.02, 0.98, 0.98], projection='polar')

    all_angles = np.arange(0, 200, 5)
    for phase in phase_filter:
        grp = df[df.phase == phase].groupby(['angle']).mean()

        y = []
        for angle in all_angles:
            rssi = grp.rssi[grp.index == angle].values
            if len(rssi) == 1:
                rssi = rssi[0]
            else:
                rssi = -100
            y.append(rssi)



        a = all_angles
        a = a / 200 * 2 * np.pi
        a = np.append(a, 2 * np.pi)
        y = np.append(y, y[0])

        ax.plot(a, y, linewidth=4)

    plt.ylim(min(df.rssi) * 1.05, max(df.rssi) *.95)
    plt.yticks(np.arange(np.floor(min(df.rssi) * 1.05 / 10) * 10, np.ceil(max(df.rssi) * .95 / 10) * 10, 10))

    return fig

def plot_xy_pattern(df, power_level, phase_filter, figure = None):
    df = df[df.power == power_level]
    if figure is None:
        fig = plt.figure()
    else:
        fig = figure
    #ax = fig.add_axes([0.02, 0.02, 0.98, 0.98], projection='polar')

    for phase in phase_filter:
        grp = df[df.phase == phase].groupby(['angle']).mean()
        a = np.array(grp.index.values)
        a = a / 200 * 360
        y = grp.rssi.values

        plt.plot(a, y)

    plt.ylim(min(df.rssi) * 1.15, max(df.rssi) *.95)
    plt.xlim([0, 360])
    #plt.yticks(np.arange(np.floor(min(df.rssi) * 1.05 / 10) * 10, np.ceil(max(df.rssi) * .95 / 10) * 10, 10))
    plt.legend([str(p) for p in phase_filter], ncol = int(len(phase_filter) / 2), loc='lower right')
    plt.xlabel('Physical angle (degrees)')
    plt.ylabel('RSSI (dBm)')

    return fig


def ingest(fn):
    f = open(fn, 'r')
    lines = f.readlines()
    f.close()

    data = []
    for line_num in range(1, len(lines)):
        try:
            line = lines[line_num].strip()
            tuples = line.split(',')
            tuples = [int(t) for t in tuples]
            (seq,power,angle,phase,prr,rssi,lqi,rssi_std,lqi_std) = tuples
            rssi = rssi - 45
            data.append((seq,power,angle,phase,prr,rssi,lqi,rssi_std,lqi_std))
        except:
            raise Exception('Failed in line %s' % line_num )

    df = pd.DataFrame(data)
    df.columns = ('seq','power','angle','phase','prr', 'rssi','lqi','rssi_std','lqi_std')
    return df


if __name__ == "__main__":
    # wo5m = ingest('/Users/ochipara/Working/DirectionalAntennas/data/leo/data/Wo_5m.csv')
    #plot_pattern(wo5m, 31, [0])
    # plot_xy_pattern(wo5m, 31, [0])

    # telos_mlh = ingest('/Users/ochipara/Working/DirectionalAntennas/data/leo/data/telosb_mlh.csv')
    # for power in np.unique(telos_mlh.power):
    #     plot_pattern(wo5m, power, [0])

    wo3m = ingest('/Users/ochipara/Working/DirectionalAntennas/data/leo/data/Wo_3m.csv')
    f = plot_pattern2(wo3m, 3, [192, 64])
    plt.show()