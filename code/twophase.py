import sys
import re
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import leastsq


def benefit(df):
    #print(df.describe())
    df_angle = df.groupby(['angle'])
    rssi_min = df_angle.min()['rssi_avg']
    rssi_max = df_angle.max()['rssi_avg']
    x = rssi_min.index.values / 200 * 2 * np.pi
    y = rssi_max.values - rssi_min.values
    x = np.append(x, 2 * np.pi)
    y = np.append(y, y[0])
    return x, y


def plot_benefit(df, plot=True):
    df_phase1 = df[df.phase1 == 0]
    df_phase2 = df[df.phase2 == 0]
    df_phase12 = df

    x, y1 = benefit(df_phase1)
    x, y2 = benefit(df_phase2)
    x, y12 = benefit(df_phase12)

    if plot:
        fig = plt.figure()
        ax = fig.add_axes([0.02, 0.02, 0.98, 0.98], projection='polar')
        ax.plot(x, y1, 'r-')
        ax.plot(x, y2, 'b-')
        ax.plot(x, y12, 'k-')
        plt.legend(['$\phi_1$', '$\phi_2$', '$\phi_1, \phi_2$'], bbox_to_anchor=(1.1, 1.05))

    return (x, y1, y2, y12)


def plot_telos_pattern(df, figure=None):
    a = np.array(df.angle)
    a = a / 200 * 2 * np.pi
    a = np.append(a, 2 * np.pi)
    y = df.rssi_avg.values
    y = np.append(y, y[0])

    if figure is None:
        fig = plt.figure()
    else:
        fig = figure
    ax = fig.add_axes([0.02, 0.02, 0.98, 0.98], projection='polar')
    ax.plot(a, y)
    ax.set_rmax(max(y))
    ax.set_rmin(min(y))
    return fig


def plot_antenna_pattern(df, phase1_range=None, phase2_range=None, figure=None, ls=None, lw=2, metric=None):
    g = df.groupby(['phase1', 'phase2'])
    if figure is None:
        fig = plt.figure()
    else:
        fig = figure
    ax = fig.add_axes([0.02, 0.02, 0.98, 0.98], projection='polar')

    pairs = []
    for index, group in g:
        phase1, phase2 = index

        if phase1 is not None and phase1 not in phase1_range:
            continue
        if phase2 is not None and phase2 not in phase2_range:
            continue

        pairs.append(index)
        a = np.array(group.angle)
        a = a / 200 * 2 * np.pi
        a = np.append(a, a[0])

        if metric is None or metric == 'rssi':
            y = group.rssi_avg.values
        elif metric == 'lqi':
            y = group.lqi_avg.values
        else:
            raise ValueError('Invalid metric')
        y = np.append(y, y[0])
        if ls is None:
            ax.plot(a, y, linewidth=lw)
        else:
            ax.plot(a, y, ls, linewidth=lw)

    if metric is None or metric == 'rssi':
        max_val = max(df.rssi_avg)
        min_val = min(df.rssi_avg)
    elif metric == 'lqi':
        max_val = max(df.lqi_avg)
        min_val = min(df.lqi_avg)


    ax.set_rmax(max_val)
    ax.set_rmin(min_val)
    print(min_val, max_val)


    return fig, pairs


def plot_antenna_pattern2(df):
    df_phase1 = df[df.phase1 == 0]
    df_phase2 = df[df.phase2 == 0]
    df_phase12 = df

    plot_antenna_pattern(df_phase1)
    plot_antenna_pattern(df_phase2)
    plot_antenna_pattern(df_phase12)


def polyarea(x,y):
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))


def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)


def polarunion(x, y1, y2):
    y = []
    for index in range(len(y1)):
        y.append(max(y1[index], y2[index]))
    return y


def coverage(x, y1, y2, y12):
    y3 = polarunion(x, y1, y2)
    (a3, b) = pol2cart(y3, x)
    (a12, b) = pol2cart(y12, x)
    big = polyarea(a12,b)
    approx = polyarea(a3,b)
    return approx / big * 100.0


def telos_gains(df_phaser, df_telos, plot=False):
    grp = df_phaser.groupby(['angle'])
    t1 = grp.rssi_avg.max()
    t2 = df_telos[['angle', 'rssi_avg']]
    t2 = t2.set_index('angle')
    jt = pd.concat([t1, t2], axis=1)
    jt.columns = ['phaser_rssi', 'telosb_rssi']
    gain = jt.phaser_rssi - jt.telosb_rssi
    gain.columns = ['gain']
    jt = pd.concat([jt, gain], axis=1)
    jt.columns = ['phaser_rssi', 'telosb_rssi', 'gain']

    if plot:
        fig = plt.figure()
        ax = fig.add_axes([0.02, 0.02, 0.98, 0.98], projection='polar')
        angles = jt.index.values / 200 * 2 * np.pi
        g = jt.gain.values
        ax.plot(angles, g)
        ax.set_rmax(max(g))
        ax.set_rmin(min(g))

    return jt


def telos_interf_benefit(df):
    missing = set()
    angles = np.unique(df.angle)
    benefit = {}
    for sender_angle in angles:
        sender_rssi = df.rssi_avg[df.angle == sender_angle].values[0]

        for interf_angle in angles:
            interf_rssi = df.rssi_avg[df.angle == sender_angle].values[0]

            snr = sender_rssi - interf_rssi
            k = '%d-%d' % (sender_angle, interf_angle)
            if k not in benefit:
                benefit[k] = snr
            elif benefit[k] < snr:
                benefit[k] = snr


    print('missing:', sorted(missing))
    return benefit


def interf_benefit(df, phase1_range=None, phase2_range=None):
    angles = np.unique(df.angle)
    lookup = {}

    df_small = df[['angle', 'phase1', 'phase2', 'rssi_avg']]
    g = df_small.groupby(['angle', 'phase1', 'phase2'])
    phases = set([])
    for index,values in g:
        k = '%d-%d-%d' % index
        phases.add((index[1], index[2]))
        lookup[k] = values.rssi_avg.mean()

    benefit = {}
    configuration = {}
    missing = set()
    for phase1, phase2 in phases:
        if phase1_range is not None:
            if phase1 not in phase1_range: continue
        if phase2_range is not None:
            if phase2 not in phase2_range: continue

        for sender_angle in angles:
            sender_key = '%d-%d-%d' % (sender_angle, phase1, phase2)
            if sender_key not in lookup:
                missing.add((sender_angle, phase1, phase2))
                continue
            sender_rssi = lookup[sender_key]
            for interf_angle in angles:
                interf_key = '%d-%d-%d' % (interf_angle, phase1, phase2)
                if interf_key not in lookup:
                    missing.add((interf_angle, phase1, phase2))
                    continue
                interf_rssi = lookup[interf_key]

                snr = sender_rssi - interf_rssi
                k = '%d-%d' % (sender_angle, interf_angle)
                if k not in benefit:
                    benefit[k] = snr
                    configuration[k] = (sender_rssi, interf_rssi, phase1, phase2)
                elif benefit[k] < snr:
                    benefit[k] = snr
                    configuration[k] = (sender_rssi, interf_rssi, phase1, phase2)

    if len(missing) > 0:
        print('warning: found missing keys', sorted(missing))
    return benefit, configuration


def plot_interf_benefit(all_benefit, sender):
    angles = np.arange(0, 200, 5)
    benefit = []
    for interf in angles:
        benefit.append(all_benefit['%d-%d' % (sender, interf)])

    x = angles / 200 * 2 * np.pi;
    y = benefit;
    x = np.append(x, 2 * np.pi)
    y = np.append(y, y[0])

    fig = plt.figure()
    ax = fig.add_axes([0.02, 0.02, 0.98, 0.98], projection='polar')
    plt.plot(x,y)
    plt.plot([0, 0], [0, max(y)], 'k-', linewidth=3)
    #plt.plot([0, np.pi / 2], [0, max(y)], 'k-', linewidth=3)
    ax.text(-0.01, max(y)-2, 'R', bbox=dict(facecolor='red'), fontsize=16)
    #ax.text((98 / 360) * np.pi * 2, max(y), 'Intrf', bbox=dict(facecolor='g'), fontsize=16)

    return fig


def compute_directional_benefit(df, power_level, phase1_range=None, phase2_range=None):
    df2 = df[df.power == power_level]
    if phase1_range is not None:
        x = df2.phase1.isin(phase1_range)
        df2 = df2[x]

    if phase2_range is not None:
        x = df2.phase2.isin(phase2_range)
        df2 = df2[x]

    g = df2.groupby(['angle'])
    r = {}
    for angle, value in g:
        max_val = value.rssi_avg.max()
        min_val = value.rssi_avg.min()
        #mean_val = value.rssi_avg.mean()
        r[angle] = max_val - min_val

    x = sorted(list(r.keys()))
    y = [r[k] for k in x]
    B = pd.DataFrame({'angle': x, 'benefit': y})
    return B


def testbed_directional_benefit(dir, key_prefix):
    data = {}
    for node in range(16):
        key = '%s-node%d' % (key_prefix, node)
        df = pd.DataFrame.from_csv('%s%d.csv' % (dir, node))
        data[key] = compute_directional_benefit(df, 31)
    return data





def compute_isotropy(df, phaseA=None, phaseB=None):
    if phaseA is not None:
        x = df.phase1.isin(phaseA)
        df = df[x]

    if phaseB is not None:
        x = df.phase2.isin(phaseB)
        df = df[x]

    outer = df.groupby(['angle']).rssi_avg.max()
    angles = outer.index.values / 200 * 2 * np.pi
    rssi = outer.values

    isotropy = np.std(rssi - np.std(rssi))
    return isotropy, angles

if __name__ == "__main__":
    fn = sys.argv[1]
    #fn = '/Users/ochipara/Working/DirectionalAntennas/data/testbed/biphase/2016-09-22_NODE1.txt'
    print('file', fn)
    df = pd.DataFrame.from_csv(fn)
    #missing = interf_benefit(df)
    #print(missing)
    compute_directional_benefit(df, 31, phase1_range=[0])
    #sys.exit(0)
    plot_antenna_pattern(df, phase1_range=np.arange(0, 255, 32), phase2_range=np.arange(0, 255, 32))

    #plot_antenna_pattern2(df)
    plt.savefig('twophases_pattern.pdf')

    (x, y1, y2, y12) = plot_benefit(df)
    plt.savefig('twophases_benefit.pdf')

    c = coverage(x, y1, y2, y12)
    print('coverage', c)

    plt.show()
