from temporalanalysis.temporal_analysis_ab import compute_avg_behavior, testbed_phase_matrix
import itertools
import numpy as np
import os
import pickle
import pandas as pd


def check_spatial_reuse(sender, interferers, rssi, dmin):
    for r in range(8):
        for c in range(8):
            sender_rssi = rssi[sender][r, c]

            reuse = True
            for interf in interferers:
                interf_rssi = rssi[interf][r, c]
                d = sender_rssi - interf_rssi
                if d < dmin:
                    reuse = False
                    break
            if reuse:
                return True
    return False


def compute_spatial_reuse(nmax, dmin, rssi, dir):
    fn_spatial_reuse = '%s/spatial_reuse_%d_%d.pickle' % (dir, nmax, dmin)
    if os.path.isfile(fn_spatial_reuse):
        with open(fn_spatial_reuse, 'rb') as f:
            (reuse, total) = pickle.load(f)
            return reuse, total
    print('Computng', nmax, dmin)
    reuse = {}
    total = {}
    for n in range(2, nmax):
        reuse[n] = []
        count = 0
        for nodeset in itertools.combinations(range(16), n):
            for sender in nodeset:
                interf = set(nodeset).difference([sender])
                count += 1

                if check_spatial_reuse(sender, interf, rssi, dmin):
                    reuse[n].append((sender, interf))
        total[n] = count

    with open(fn_spatial_reuse, 'wb') as f:
        pickle.dump((reuse,total), f, pickle.HIGHEST_PROTOCOL)

    return reuse, total


def compute_spatial_reuse_for_antenna(d, angles, phases, dth):
    covered = set([])
    for phase1 in phases:
        for phase2 in phases:
            for sender in angles:
                ksender = '%d-%d-%d' % (phase1, phase2, sender)
                if ksender not in d: continue
                sender_rssi = d[ksender]
                for recv in angles:
                    krecv = '%d-%d-%d' % (phase1, phase2, recv)
                    if krecv not in d: continue
                    recv_rssi = d[krecv]
                    if sender_rssi - recv_rssi > dth:
                        # covered
                        covered.add((sender, recv))

    F = len(covered) / (len(angles) * len(angles)) * 100
    return F, covered

def testbed_index_rssi(dir, prefix):
    fn_spatial_reuse = '%s/index.pickle' % (dir)
    if os.path.isfile(fn_spatial_reuse):
        with open(fn_spatial_reuse, 'rb') as f:
            index = pickle.load(f)
            return index

    index = {}
    for node in range(16):
        fn = '%s/%s%d.csv' % (dir, prefix, node)
        print('Indexing node', node)
        df = pd.DataFrame.from_csv(fn)

        df = df[['angle', 'phase1', 'phase2', 'rssi_avg']]
        g = df.groupby(['phase1', 'phase2', 'angle'])
        d = {}
        for config, value in g:
            k = '%d-%d-%d' % config
            d[k] = np.mean(value.rssi_avg)
        index[node] = d

    with open(fn_spatial_reuse, 'wb') as f:
        pickle.dump(index, f, pickle.HIGHEST_PROTOCOL)

    return index

if __name__ == "__main__":
    index = testbed_index_rssi('/Users/ochipara/Working/DirectionalAntennas/data/testbed/tx_patterns/MoteF_AntennaM/', '2016-09-27_NODE')
    angles = np.arange(0, 200, 5)
    phases = np.arange(0, 256, 32)
    F, covered = compute_spatial_reuse_for_antenna(index[0], angles, phases, 6)
    print(F)
    # fn = '/Users/ochipara/Working/DirectionalAntennas/data/testbed/tx_patterns/MoteF_AntennaM/2016-09-27_NODE0.csv'
    # df = pd.DataFrame.from_csv(fn)
    # F = compute_spatial_reuse_for_antenna(df, 6)
    # print(F)



    # dir = '/Users/ochipara/Working/DirectionalAntennas/data/testbed/tx_temporal/Ch11_Pwr7_MoteA/'
    # C = testbed_phase_matrix(dir, '2016-10-05_NODE')
    #
    # for dmin in range(4, 20, 4):
    #     print(dmin)
    #     reuse, total = compute_spatial_reuse(8, dmin, C, dir)
    #
    # for k in reuse:
    #     print(k, reuse[k])

