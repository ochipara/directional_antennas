"""
Plots the results on the testbed
"""
import matplotlib.pyplot as plt
import numpy as np


def plot_testbed(vals_map):
# 	node2index = {
# 		15 : (3, 0),
# 		12 : (3, 1),
# 		3  : (3, 2),
# 		0  : (3, 3),
# 		14 : (2, 0),
# 		13 : (2, 1),
# 		2  : (2, 2),
# 		1  : (2, 3),
# 		11 : (1, 0),
# 		8  : (1, 1),
# 		7  : (1, 2),
# 		4  : (1, 3),
# 		10 : (0, 0),
# 		9  : (0, 1),
# 		6  : (0, 2),
# 		5  : (0, 3)
# 	}	
	node2index = {
		15 : (0, 3),
		12 : (1, 3),
		3  : (2, 3),
		0  : (3, 3),
		14 : (0, 2),
		13 : (1, 2),
		2  : (2, 2),
		1  : (3, 2),
		11 : (0, 1),
		8  : (1, 1),
		7  : (2, 1),
		4  : (3, 1),
		10 : (0, 0),
		9  : (1, 0),
		6  : (2, 0),
		5  : (3, 0)
	}		
	c = np.zeros((4, 4), dtype=float)
	for node in vals_map:		
		index = node2index[node]
		c[index] = vals_map[node]
	pc = plt.pcolor(c)
	
	for (node, loc) in node2index.items():
		plt.text(loc[0] + 0.5, loc[1] + 0.5, '%d' % node)
	plt.colorbar(pc)
		
if __name__ == "__main__":
	plt.figure()
	
	T = {15: 1, 12 : 1, 3 : 1, 0 : 1, 14 : 1, 13 : 1, 2: 1, 1:1, 11:1,8:1,7:1,4:1, 10:1,9:1, 6:1, 5:1}
	plot_testbed(T)

	plt.show()