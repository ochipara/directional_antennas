from antenna_pattern import *
from directional_benefit import *

if __name__ == "__main__":
    e_santa = load_experiment('../data/stadium/log_2016-07-18_stadium_1santa_2.csv')
    df_santa = e_santa.get_dataframe()

    all_phases = np.unique(df_santa.phase)
    print('unique phases', np.unique(all_phases), len(all_phases))
    all_phases = list(all_phases)
    all_phases.remove(63)
    plot_antenna_pattern(e_santa, all_phases, missing='nan', debug=False)

    benefit_santa = compute_directional_benefit(df_santa, 31)
    plot_directional_benefit(benefit_santa)

    e_phaser = load_experiment('../data/stadium/log_2016-07-13_stadium_1a_1.csv')
    df_phaser = e_phaser.get_dataframe()
    all_phases = np.unique(df_santa.phase)
    print('unique phases', np.unique(all_phases), len(all_phases))
    all_phases = list(all_phases)
    plot_antenna_pattern(e_phaser, all_phases, missing='nan', debug=False)

    benefit_phasor = compute_directional_benefit(df_phaser, 31)
    plot_directional_benefit(benefit_phasor)






    print('Santa benefit', np.mean(benefit_santa.values))
    print('Phasor benefit', np.mean(benefit_phasor.values))




    plt.show()
